package com.delivery.pro.entity;

import java.sql.Timestamp;
import java.util.Objects;

public class Receipt implements Comparable<Receipt> {
	public static final int PAY_STATUS_UNPAID = 1;
	public static final int PAY_STATUS_PAID = 2;
	
	public static final int ORDER_STATUS_NEW = 1;
	public static final int ORDER_STATUS_IN_PROGRESS = 2;
	public static final int ORDER_STATUS_DONE = 3;
	public static final int ORDER_STATUS_CANCELLED = 4;
	
	private int id;
	private User user;
	private Delivery delivery;
	private String type;
	private double weight;
	private String size;
	private double price;
	private Timestamp createTime;
	private Timestamp updateTime;
	private int payStatus = PAY_STATUS_UNPAID;
	private int status = ORDER_STATUS_NEW;
	private String payStatusName;
	private String statusName;
	private int category;
	private String categoryName;
	
	public Receipt() {}
	
	public static Receipt createReceipt() {
		return new Receipt();
	}
	
	public static Receipt getReceipt() {
		return new Receipt();
	}
	
	public Delivery getDelivery() {
		return delivery;
	}
	public void setDelivery(Delivery delivery) {
		this.delivery = delivery;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	
	public int getUserId() {
		return id;
	}
	public void setUserId(int id) {
		this.id = id;
	}
	
	public int getDeliveryId() {
		return id;
	}
	public void setDeliveryId(int id) {
		this.id = id;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	public Timestamp getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(Timestamp updateTime) {
		this.updateTime = updateTime;
	}
	public int getPayStatus() {
		return payStatus;
	}
	public void setPayStatus(int payStatusId) {
		this.payStatus = payStatusId;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	
    
	@Override
	public String toString() {
		return "Receipt{" 
				+ "id=" + id 
				+ ", userId=" + user.getId() 
				+ ", email=" + user.getEmail()
				+ ", deliveryId=" + delivery.getId()
				+ ", route=" + delivery.getRoute()
				+ ", type=" + type
				+ ", weight=" + weight 
				+ ", size=" + size 
				+ ", price=" + price 
				+ ", createTime=" + createTime
				+ ", updateTime=" + updateTime 
				+ ", payStatusId=" + payStatus 
				+ ", payStatusName=" + payStatusName 
				+ ", statusId=" + status
				+ ", statusName=" + statusName
				+ ", categoryId=" + category
				+ ", categoryName=" + categoryName
				+ "}";
	}
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Receipt receipt = (Receipt) o;
        return id == receipt.id && createTime.equals(receipt.createTime) && user.equals(receipt.user);
    }
    
    @Override
    public int hashCode() {
        return Objects.hash(id, createTime, user);
    }

    @Override
    public int compareTo(Receipt o) {
        return o.createTime.compareTo(createTime);
    }

	public String getPayStatusName() {
		return payStatusName;
	}

	public void setPayStatusName(String payStatusName) {
		this.payStatusName = payStatusName;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	public int getCategory() {
		return category;
	}

	public void setCategory(int category) {
		this.category = category;
	}
	public String getCategoryName() {
    	return categoryName;
    }
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
		
	}
}