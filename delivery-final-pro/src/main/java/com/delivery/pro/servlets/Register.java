package com.delivery.pro.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.dao.UserDAO;
import com.delivery.pro.entity.User;

import at.favre.lib.crypto.bcrypt.BCrypt;

@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(Register.class);

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		String hashPass = BCrypt.withDefaults().hashToString(12, request.getParameter("password").toCharArray());
		if(user == null) {
			user = User.createUser();
			user.setFirstName(request.getParameter("firstName"));
			user.setLastName(request.getParameter("lastName"));
			user.setEmail(request.getParameter("email"));
			user.setPassword(hashPass);
			session.setAttribute("user", user);
			//request.setCharacterEncoding("UTF-8");
			//response.setContentType("text/html; charset=UTF-8");
			//response.setCharacterEncoding("UTF-8");
		}
		if (UserDAO.validateUser(user)) {
			session.invalidate();
			log.info("Email " + user.getEmail() + " is already in the database, redirect to login page");
			response.sendRedirect("login.jsp");
		} else {
			UserDAO.insertUser(user);
			log.info("Email " + user.getEmail() + " has been saved");
			session.invalidate();
			response.sendRedirect("registration_success.jsp");
		}
	}
}
