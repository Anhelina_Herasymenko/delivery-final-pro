package com.delivery.pro.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.delivery.pro.dao.DeliveryDAO;
import com.delivery.pro.dao.UserDAO;
import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.Receipt;

@WebServlet("/getOrderInfo")
public class GetOrderInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Receipt receipt = null;
		HttpSession session = request.getSession();
		if(session.getAttribute("user") == null) {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		int receiptId = Integer.parseInt(request.getParameter("receiptId"));
		
		Connection conn = null;
		try {
			conn = DBManager.getInstance().getConnection();
			PreparedStatement pstmt = conn.prepareStatement("SELECT distinct receipt.id, user.id, user.email, delivery_id, delivery.route, receipt.type, receipt.weight, receipt.size, receipt.price, receipt.create_time, receipt.update_time, pay_status.name, status.name "
					+ "FROM user, receipt, delivery, status, pay_status "
					+ "WHERE user_id = user.id and delivery_id = delivery.id and receipt.status_id = status.id and receipt.pay_status_id = pay_status.id AND receipt.id=?");
			pstmt.setInt(1, receiptId);
			ResultSet rs = pstmt.executeQuery();
			
			if (rs.next()) {
				receipt = Receipt.createReceipt();
				receipt = new Receipt();
				receipt.setId(rs.getInt("id"));
				receipt.setUser(UserDAO.getUserId(rs.getInt("id")));
				receipt.setUser(UserDAO.getUser(rs.getString("email")));
				receipt.setDelivery(DeliveryDAO.getDeliveryId(rs.getInt("id")));
				receipt.setDelivery(DeliveryDAO.getDeliveryByRoute(rs.getString("route")));
				receipt.setType(rs.getString("type"));
				receipt.setWeight(rs.getDouble("weight"));
				receipt.setSize(rs.getString("size"));
				receipt.setPrice(rs.getDouble("price"));
				receipt.setCreateTime(rs.getTimestamp("create_time"));
				receipt.setUpdateTime(rs.getTimestamp("update_time"));
				receipt.setPayStatusName(rs.getString("pay_status.name"));
				receipt.setStatusName(rs.getString("status.name"));
			}
			conn.close();
			rs.close();
			pstmt.close();
		} catch (SQLException  ex) { 
			ex.printStackTrace();
		}
		
		request.setAttribute("receipt", receipt);
		request.getRequestDispatcher("/manager/get_info.jsp").forward(request, response);
		
	}
}