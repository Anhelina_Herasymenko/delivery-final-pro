package com.delivery.pro.dao;

import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.User;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

public class UserDAOTest {
	DBManager dbManager;
	String testEmail = "email";
	
	@BeforeEach
	public void getConnection() {
		dbManager = DBManager.getInstance(true);
	}
	
	@Test
	public void shouldGetUserByEmail() {
		User user = User.createUser();
		user.setEmail("test001@gmail.com");
		user.setFirstName("Test001");
		user.setLastName("Test");
		user.setPassword("test");
		user.setRole(2);
		UserDAO.insertUser(user);
		
		User userCheck = UserDAO.getUser("test@gmail.com");
		assertEquals("test@gmail.com", userCheck.getEmail());
	}
	
	@Test
	public void shouldInsertNewUser() {
		User user = User.createUser();
		user.setEmail("test001@gmail.com");
		user.setFirstName("Test001");
		user.setLastName("Test");
		user.setPassword("test");
		UserDAO.insertUser(user);
		User userCheck = UserDAO.getUser(user.getEmail());
		assertEquals(user, userCheck);
	}
	
	@Test
	public void shouldGetUserList() {
		List<User> userList;
		User user = User.createUser();
		user.setEmail("test001@gmail.com");
		user.setFirstName("Test001");
		user.setLastName("Test");
		user.setPassword("test");
		UserDAO.insertUser(user);
		userList = UserDAO.getAllUsers();
		
		assertFalse(userList.isEmpty());
	}
	
	@AfterEach
	public void deleteTestUser() {
		User userCheck = UserDAO.getUser("test001@gmail.com");
		if(userCheck.getEmail()!=null) {
			UserDAO.deleteUserByEmail("test001@gmail.com");
		}
	}
}
