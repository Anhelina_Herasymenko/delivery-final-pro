package com.delivery.pro.servlets;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.delivery.pro.dao.DeliveryDAO;
import com.delivery.pro.entity.Delivery;

@WebServlet("/loadDeliveryList")
public class LoadDeliveryList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Delivery> deliveryArray;
		Map<Integer, String> deliveryCategory = DeliveryDAO.getAllCategory();
		
		Map<String, String> sortBy = new HashMap<>();
        sortBy.put("Route", "route");
        
        request.setAttribute("deliverySorting", sortBy);
        request.setAttribute("deliveryCategory", deliveryCategory);
        
        int deliveryPerPage = 5;
        int deliveriesByCategory = 1;
        
		if(request.getParameter("deliveryCategoryId") != null) {
			deliveriesByCategory = DeliveryDAO.getDeliveryCount(Integer.parseInt(request.getParameter("deliveryCategoryId")));
		}
        
		int numOfPages = (int) Math.ceil((double) deliveriesByCategory/deliveryPerPage);
		request.setAttribute("numOfPages", numOfPages);
		request.setAttribute("deliveryPerPage", deliveryPerPage);
		
		if(request.getParameter("deliveryCategoryId") == null && request.getParameter("deliverySort") == null && request.getParameter("currentPage") == null) {
			request.setAttribute("deliverySort", "id");
			request.setAttribute("deliveryCategoryId", 1);
			request.setAttribute("currentPage", 1);
			deliveryArray = DeliveryDAO.getAllDelivery(1, "id", 0, deliveryPerPage);
		} else if(request.getParameter("deliveryCategoryId") != null && request.getParameter("deliverySort") != null && request.getParameter("currentPage") != null) {
			request.setAttribute("currentPage", request.getParameter("currentPage"));
			request.setAttribute("deliverySort", request.getParameter("deliverySort"));
			request.setAttribute("deliveryCategoryId", request.getParameter("deliveryCategoryId"));
			
			int currentPage = Integer.parseInt(request.getParameter("currentPage"));
			String deliverySort = request.getParameter("deliverySort");
			int deliveryCategoryId = Integer.parseInt(request.getParameter("deliveryCategoryId"));
			
			deliveryArray = DeliveryDAO.getAllDelivery(deliveryCategoryId, deliverySort, (currentPage-1)*deliveryPerPage, deliveryPerPage);
		} else {
			deliveryArray = DeliveryDAO.getAllDelivery(1, "id", 0, deliveryPerPage);
		}
		request.setAttribute("deliveryArray", deliveryArray);
		request.getRequestDispatcher("listofdelivery.jsp").forward(request, response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
