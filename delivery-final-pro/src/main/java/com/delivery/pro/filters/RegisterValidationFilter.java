package com.delivery.pro.filters;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebFilter(urlPatterns = { "/Register", "/Login" })
public class RegisterValidationFilter implements Filter {
	private static final Logger log = LogManager.getLogger(RegisterValidationFilter.class);
	FilterConfig filterConfig;
	private static String firstNameRegex = "^[а-яА-Яa-zA-ZàáâäãåąčćęèéêëėįìíîїіłńòóôöõøùúûüųūÿýżźñçčšžйєÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎЇІĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹЙЄÑßÇŒÆČŠŽ∂ð ,.'-]+$";
	private static String lastNameRegex = "^[а-яА-Яa-zA-ZàáâäãåąčćęèéêëėįìíîїіłńòóôöõøùúûüųūÿýżźñçčšžйєÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎЇІĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹЙЄÑßÇŒÆČŠŽ∂ð ,.'-]+$";
	private static String emailRegex = "^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$";

	public void init(FilterConfig fConfig) throws ServletException {
		filterConfig = fConfig;
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletResponse responseSERV = (HttpServletResponse) response;
		log.info("DO FILTER FOR REGISTRATION VALIDATION");

		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String password = request.getParameter("password");
		String email = request.getParameter("email");

		if (request.getParameter("registration_form") != null) {
			if (password == null || "".equals(password) || email == null || "".equals(email)) {
				responseSERV.sendRedirect("error_pages/registration_error.jsp");
			} else if (email.matches(emailRegex) && firstName.matches(firstNameRegex)
					&& lastName.matches(lastNameRegex)) {
				chain.doFilter(request, response);
			} else if (firstName.isBlank() || lastName.isBlank()) {
				responseSERV.sendRedirect("error_pages/registration_error.jsp");
			} else {
				responseSERV.sendRedirect("error_pages/registration_error.jsp");
			}
		}

		if (request.getParameter("log_in_form") != null) {
			if (email == null || "".equals(email) || password == null || "".equals(password)) {
				responseSERV.sendRedirect("error_pages/login_error.jsp");
			} else if (email.matches(emailRegex)) {
				chain.doFilter(request, response);
			} else {
				responseSERV.sendRedirect("error_pages/login_error.jsp");
			}
		}
	}
}
