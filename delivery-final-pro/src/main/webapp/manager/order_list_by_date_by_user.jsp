<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">
<c:set var="title" value="All deliveries" scope="page"/>
<head>
<title><fmt:message key="manager_page.users_orders"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_manager2.jsp"/>
</head>
<body>
<h3>

<div>User ID<input name="userId" placeholder="1" required/> <br>

<label for="start">Start date:</label>
<input type="text" name="createTimeFrom" value="2021-01-15" required>
<label for="start">End date:</label>
<input type="text" name="createTimeTo" value="2021-01-16" required>
<a href="${pageContext.request.contextPath}/getUserOrderInfoByDate?userId=${requestScope.userId}&createTimeFrom=${requestScope.createTimeFrom}&createTimeTo=${requestScope.createTimeTo}">
            <input type="submit" value="Submit"/></a></div>
    
    <table id="information" border="1">
        <tr>
            <th>ID</th>
			<th><fmt:message key="manager_page.user_id"/></th>
            <th><fmt:message key="manager_page.user_email"/></th>
            <th><fmt:message key="manager_page.delivery_id"/></th> 
            <th><fmt:message key="manager_page.delivery_route"/></th>
            <th><fmt:message key="manager_page.type"/></th>
            <th><fmt:message key="manager_page.weight"/></th>
            <th><fmt:message key="manager_page.size"/></th>
            <th><fmt:message key="manager_page.price"/></th>
            <th><fmt:message key="manager_page.create"/></th>
            <th><fmt:message key="manager_page.update"/></th>
            <th><fmt:message key="manager_page.pay_status"/></th>
            <th><fmt:message key="manager_page.statusid"/></th>
        </tr>
        <c:forEach items="${receipts}" var="receiptList" >
            <tr>
            <td><c:out value="${receiptList.id}" /></td>
            <td><c:out value="${receiptList.user.id}" /></td>
            <td><c:out value="${receiptList.user.email}" /></td>
            <td><c:out value="${receiptList.delivery.id}" /></td>
            <td><c:out value="${receiptList.delivery.route}" /></td>
            <td><c:out value="${receiptList.type}" /></td>
            <td><c:out value="${receiptList.weight}" /></td>
            <td><c:out value="${receiptList.size}" /></td>
            <td><c:out value="${receiptList.price}" /></td>
            <td><c:out value="${receiptList.createTime}" /></td>
            <td><c:out value="${receiptList.updateTime}" /></td>
            <td><c:out value="${receiptList.payStatus}" /></td>
            <td><c:out value="${receiptList.status}" /></td>
            </tr>
        </c:forEach>
       </table>
</h3>
<a href="${pageContext.request.contextPath}/manager/manager_profile.jsp"><fmt:message key="manager_page.return_profile"/></a><br>
</body>
</html>