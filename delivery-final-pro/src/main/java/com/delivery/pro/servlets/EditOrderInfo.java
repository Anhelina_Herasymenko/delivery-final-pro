package com.delivery.pro.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.dao.ReceiptDAO;
import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.Receipt;

@WebServlet("/editOrderInfo")
public class EditOrderInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(EditOrderInfo.class);
	   
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Receipt receipt = null;
		int receiptId = Integer.parseInt(request.getParameter("receiptId"));
		
		if(session.getAttribute("user") == null) {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		Connection conn = null;
		try {
			conn = DBManager.getInstance().getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM receipt WHERE id=?;");
			ps.setInt(1, receiptId);
			ResultSet rs = ps.executeQuery();
			
			if (rs.next()) {
				receipt = Receipt.createReceipt();
				receipt.setId(rs.getInt("id"));
				receipt.setStatus(rs.getInt("status_id"));
			}
			conn.close();
			rs.close();
			ps.close();
		} catch (SQLException ex) {
			log.error("Cannot edit order info", ex);
		}
		request.setAttribute("receipt", receipt);
		request.getRequestDispatcher("/manager/update_order_info.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Receipt receipt = null; 
		int receiptId = Integer.parseInt(request.getParameter("receiptId"));
		
		if(receipt == null) {
			receipt = Receipt.createReceipt();
			receipt.setStatus(Integer.parseInt(request.getParameter("status_id")));
			receipt.setId(receiptId);
			session.setAttribute("receipt", receipt);
		}
		ReceiptDAO.editOrderInfo(receipt);
		response.sendRedirect("manager/result_of_updating.jsp");
	}
}
