package com.delivery.pro.dao;

import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.Delivery;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class DeliveryDAOTest {
	DBManager dbManager;
	String testDeliveryRoute = "test_delivery";
	
	@BeforeEach
	public void getConnection() {
		dbManager = DBManager.getInstance(true);
	}
	
	@Test
	public void shouldAddDeliveryToDB() {
		Delivery testDelivery = Delivery.createDelivery();
		testDelivery.setRoute("test MIDSIZE 5...10");
		testDelivery.setWeightLimit("5...10");
		testDelivery.setSize("MIDSIZE");
		testDelivery.setTarif(42.50);
		testDelivery.setDescription("test");
		testDelivery.setCategory(1);
		DeliveryDAO.addDeliveryToDB(testDelivery);
		Delivery deliveryCheck = DeliveryDAO.getDeliveryByRoute(testDelivery.getRoute());
		assertEquals(testDelivery, deliveryCheck);
	}
	
	@AfterEach
	public void removeDelivery() {
		Delivery deliveryRemove = Delivery.createDelivery();
		deliveryRemove.setRoute(testDeliveryRoute);
		DeliveryDAO.deleteDelivery(deliveryRemove);
	}
	
	@Test
	public void shouldSelectDeliveryByRoute() {
		Delivery testDelivery = Delivery.createDelivery();
		testDelivery.setRoute("test MIDSIZE 5...10");
		testDelivery.setWeightLimit("5...10");
		testDelivery.setSize("MIDSIZE");
		testDelivery.setTarif(42.50);
		testDelivery.setDescription("test");
		testDelivery.setCategory(1);
		DeliveryDAO.addDeliveryToDB(testDelivery);
		Delivery deliveryCheck = DeliveryDAO.getDeliveryByRoute("test MIDSIZE 5...10");
		assertEquals(testDelivery, deliveryCheck);
	}
	
	@Test
	public void shouldEditDelivery() {
		Delivery testDelivery = Delivery.createDelivery();
		testDelivery.setRoute("test MIDSIZE 5...10");
		testDelivery.setWeightLimit("5...10");
		testDelivery.setSize("MIDSIZE");
		testDelivery.setTarif(42.50);
		testDelivery.setDescription("test");
		testDelivery.setCategory(1);
		
		double tarifBefore = testDelivery.getTarif();
		
		DeliveryDAO.addDeliveryToDB(testDelivery);
		
		Delivery deliveryFromDB = DeliveryDAO.getDeliveryByRoute("test MIDSIZE 5...10");
		deliveryFromDB.setTarif(45.50);
		DeliveryDAO.editDelivery(deliveryFromDB);
		Delivery afterEdit = DeliveryDAO.getDeliveryByRoute("test MIDSIZE 5...10");
		
		double tarifAfter = afterEdit.getTarif();
		
		assertTrue(tarifBefore < tarifAfter);
	}
	
	@Test
	public void shouldDeleteDelivery() {
		Delivery testDelivery = Delivery.createDelivery();
		testDelivery.setRoute("test MIDSIZE 5...10");
		testDelivery.setWeightLimit("5...10");
		testDelivery.setSize("MIDSIZE");
		testDelivery.setTarif(42.50);
		testDelivery.setDescription("test");
		testDelivery.setCategory(1);
		DeliveryDAO.addDeliveryToDB(testDelivery);
		
		DeliveryDAO.deleteDelivery(testDelivery);
		Delivery deliveryAfter = DeliveryDAO.getDeliveryByRoute("test MIDSIZE 5...10");
		assertNotEquals(deliveryAfter.getRoute(), testDelivery.getRoute());		
	}
}