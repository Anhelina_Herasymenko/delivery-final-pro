package com.delivery.pro.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.User;

@WebServlet("/getUserInfo")
public class GetUserInfo extends HttpServlet {

		private static final long serialVersionUID = 1L;
		
		protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			User user = null;
			HttpSession session = request.getSession();
			if(session.getAttribute("user") == null) {
				request.getRequestDispatcher("index.jsp").forward(request, response);
			}
			int userId = Integer.parseInt(request.getParameter("userId"));
			
			Connection conn = null;
			try {
				conn = DBManager.getInstance().getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM user WHERE id=?;");
				pstmt.setInt(1, userId);
				ResultSet rs = pstmt.executeQuery();
				
				if(rs.next()) {
					user = User.createUser();
					user = new User();
					user.setId(rs.getInt("id"));
					user.setFirstName(rs.getString("firstName"));
					user.setLastName(rs.getString("lastName"));
					user.setEmail(rs.getString("email"));
					user.setCreateTime(rs.getTimestamp("create_time"));
				}
				conn.close();
				rs.close();
				pstmt.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			
			request.setAttribute("user", user);
			request.getRequestDispatcher("/manager/get_user_info.jsp").forward(request, response);
		}

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			doGet(request, response);
		}
	}