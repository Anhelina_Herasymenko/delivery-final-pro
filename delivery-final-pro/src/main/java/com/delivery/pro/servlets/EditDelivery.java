package com.delivery.pro.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.dao.DeliveryDAO;
import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.Delivery;

@WebServlet("/editDelivery")
public class EditDelivery extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(EditDelivery.class);

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Delivery delivery = (Delivery) session.getAttribute("delivery"); // new

		int deliveryId = Integer.parseInt(request.getParameter("deliveryId"));
		System.out.println("deliveryId = " + deliveryId);
		if (session.getAttribute("user") == null) {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		Connection conn = null;
		try {
			conn = DBManager.getInstance().getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM delivery WHERE id=?;");
			ps.setInt(1, deliveryId);
			ResultSet rs = ps.executeQuery();

			if (rs.next()) {
				delivery = Delivery.createDelivery();
				delivery.setId(rs.getInt("id"));
				delivery.setRoute(rs.getString("route"));
				delivery.setWeightLimit(rs.getString("weight_limit"));
				delivery.setSize(rs.getString("size"));
				delivery.setTarif(rs.getDouble("tarif"));
				delivery.setDescription(rs.getString("description"));
				delivery.setCategory(rs.getInt("category_id"));
			}
			conn.close();
			rs.close();
			ps.close();
		} catch (SQLException ex) {
			log.error("Cannot edit a delivery", ex);
		}
		request.setAttribute("delivery", delivery);
		request.getRequestDispatcher("/manager/update_delivery_info.jsp").forward(request, response);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Delivery delivery = null;
		int deliveryId = Integer.parseInt(request.getParameter("deliveryId"));

		if (delivery == null) {
			delivery = Delivery.createDelivery();
			delivery.setRoute(request.getParameter("route"));
			delivery.setWeightLimit(request.getParameter("weight_limit"));
			delivery.setSize(request.getParameter("size"));
			delivery.setTarif(Double.parseDouble(request.getParameter("tarif")));
			delivery.setDescription(request.getParameter("description"));
			delivery.setCategory(Integer.parseInt(request.getParameter("category_id")));
			delivery.setId(deliveryId);
			session.setAttribute("delivery", delivery);
		}
		DeliveryDAO.editDelivery(delivery);
		log.info("Delivery " + delivery.getId() + " has been edited");
		response.sendRedirect("manager/result_of_updating.jsp");

	}
}
