<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">
<head>
<title><fmt:message key="manager_page.delivery_edit"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_manager2.jsp"/>
</head>
<body>
<div class="info">
    <p><fmt:message key="user_page.order_fields"/></p><br>
<h3>
    <form action="editDelivery" method="post">
    <table id="information" border="1">
    <tr><td>ID</td><th><input name="deliveryId" value="${delivery.id}" readonly/></th></tr>
    <tr><td><fmt:message key="manager_page.route"/></td><th><input name="route" value="${delivery.route}" required/></th></tr>
    <tr><td><fmt:message key="manager_page.weightlimit"/></td><th><input pattern="(\d*\...?\d+)\s?" name="weight_limit" value="${delivery.weightLimit}" required/></th></tr>
    <tr><td><fmt:message key="manager_page.size_edit"/></td><th><select name="size" required>
            <option>SMALLSIZE</option>
            <option>MIDSIZE</option>
            <option>LARGESIZE</option>
    </select></th></tr>
    <tr><td><fmt:message key="manager_page.tarif"/></td><th><input pattern="(\d*\.?\d+)\s?" name="tarif" value="${delivery.tarif}" required/></th></tr>
    <tr><td><fmt:message key="user_page.description"/></td><th><input name="description" value="${delivery.description}" /></th></tr>
    <tr><td><fmt:message key="manager_page.categoryid"/></td><th><input pattern="(\d*)\s?" name="category_id" value="${delivery.category}" required/></th></tr>
    <tr><td><input type="submit" value="Save" /><td/><tr/>
    </table></form>
</h3>
</div>
<br>
<a href="${pageContext.request.contextPath}/manager/manager_profile.jsp"><fmt:message key="manager_page.return_profile"/></a><br>
</body>
</html>