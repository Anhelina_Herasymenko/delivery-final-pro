package com.delivery.pro.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.dao.DeliveryDAO;
import com.delivery.pro.dao.UserDAO;
import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.Receipt;
import com.delivery.pro.entity.User;

@WebServlet("/getUserOrderInfoByDate")
public class UserOrderInfoByDate extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(UserOrderInfoByDate.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		int userId = Integer.parseInt(request.getParameter("userId"));
		if (session.getAttribute("user") == null) {
			request.getRequestDispatcher("login.jsp").forward(request, response);
		} else {
			Connection conn = null;
			User user = null;

			try {
				conn = DBManager.getInstance().getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM user WHERE id=?;");
				pstmt.setInt(1, userId);
				ResultSet rs = pstmt.executeQuery();
				if (rs.next()) {
					user = User.createUser();
					user.setId(rs.getInt("id"));
				}
				conn.close();
				rs.close();
				pstmt.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			request.setAttribute("user", user);
		}

		if (request.getParameter("createTimeFrom") == null && request.getParameter("createTimeTo") == null) {
			List<Receipt> receiptList = new ArrayList<>();
			Connection conn = null;
			ResultSet rs = null;
			try {
				conn = DBManager.getInstance().getConnection();
				PreparedStatement ps = conn.prepareStatement(
						"SELECT distinct receipt.id, user.id, user.email, delivery_id, delivery.route, receipt.type, receipt.weight, receipt.size, receipt.price, receipt.create_time, receipt.update_time,  receipt.status_id, receipt.pay_status_id "
								+ "FROM user, receipt, delivery, status, pay_status "
								+ "WHERE user.id = user_id and delivery.id = delivery_id AND user.id=?;");
				ps.setInt(1, userId);
				rs = ps.executeQuery();
				while (rs.next()) {
					receiptList.add(createReceiptUser(rs));
				}
				conn.close();
				rs.close();
				ps.close();
			} catch (SQLException ex) {
				log.error("Cannot get list of orders", ex);
			}
			request.setAttribute("receipts", receiptList);
			request.getRequestDispatcher("/manager/user_order_list_by_date.jsp").forward(request, response);
		} else {
			String createTimeFrom = request.getParameter("createTimeFrom");
			String createTimeTo = request.getParameter("createTimeTo");
			List<Receipt> receiptList = new ArrayList<>();
			Connection conn = null;
			ResultSet rs = null;
			try {
				conn = DBManager.getInstance().getConnection();
				PreparedStatement ps = conn.prepareStatement(
						"SELECT distinct receipt.id, user.id, user.email, delivery_id, delivery.route, receipt.type, receipt.weight, receipt.size, receipt.price, receipt.create_time, receipt.update_time,  receipt.status_id, receipt.pay_status_id "
								+ "FROM user, receipt, delivery, status, pay_status "
								+ "WHERE user.id = user_id and delivery.id = delivery_id AND user.id=? AND receipt.create_time BETWEEN ? AND ?;");
				ps.setInt(1, userId);
				ps.setString(2, createTimeFrom);
				ps.setString(3, createTimeTo);
				rs = ps.executeQuery();
				while (rs.next()) {
					receiptList.add(createReceiptUser(rs));
				}
				conn.close();
				rs.close();
				ps.close();
			} catch (SQLException ex) {
				log.error("Cannot get list of orders", ex);
			}
			request.setAttribute("receipts", receiptList);
			System.out.println("receipts=" + receiptList);
			System.out.println("userIdAFTER = " + userId);
			request.getRequestDispatcher("/manager/user_order_list_by_date.jsp").forward(request, response);
		}
	}

	private Receipt createReceiptUser(ResultSet rs) throws SQLException {
		Receipt receipt = Receipt.createReceipt();
		receipt.setId(rs.getInt("id"));
		receipt.setUser(UserDAO.getUserId(rs.getInt("id")));
		receipt.setUser(UserDAO.getUser(rs.getString("email")));
		receipt.setDelivery(DeliveryDAO.getDeliveryId(rs.getInt("id")));
		receipt.setDelivery(DeliveryDAO.getDeliveryByRoute(rs.getString("route")));
		receipt.setType(rs.getString("type"));
		receipt.setWeight(rs.getDouble("weight"));
		receipt.setSize(rs.getString("size"));
		receipt.setPrice(rs.getDouble("price"));
		receipt.setCreateTime(rs.getTimestamp("create_time"));
		receipt.setUpdateTime(rs.getTimestamp("update_time"));
		receipt.setPayStatus(rs.getInt("pay_status_id"));
		receipt.setStatus(rs.getInt("status_id"));
		return receipt;
	}
}
