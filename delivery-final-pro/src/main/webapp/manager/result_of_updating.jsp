<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">

<head>
<title><fmt:message key="manager_page.edition_success"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_manager.jsp"/>
</head>
<body>
<h3><fmt:message key="manager_page.edition_success"/><br></h3>
<p>
<a href="${pageContext.request.contextPath}/manager/manager_profile.jsp"><fmt:message key="manager_page.return_profile"/></a>
</p>
</body>
</html>