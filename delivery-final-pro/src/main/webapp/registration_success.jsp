<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">
<head>
    <title><fmt:message key="index_page.title"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
    <jsp:include page="/navigation/navigation_guest.jsp"/>
</head>
<body>
<div class="header" align="center">
<h3>
<fmt:message key="index_page.reg_success"/><br>
<fmt:message key="index_page.please_login"/> <a href="login.jsp"><fmt:message key="index_page.login"/></a><br>
</h3>
</div>
</body>
</html>