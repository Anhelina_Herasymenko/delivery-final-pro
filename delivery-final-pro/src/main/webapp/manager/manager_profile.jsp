<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">

<head>
<title><fmt:message key="manager_page.profile"/></title>
<jsp:include page="/navigation/navigation_manager.jsp"/>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
</head>
<body>
<h2><fmt:message key="manager_page.profile"/></h2>
<a href="${pageContext.request.contextPath}/UserList"><fmt:message key="manager_page.user_list"/></a><br>
<a href="${pageContext.request.contextPath}/loadOrdersManager"><fmt:message key="manager_page.receipt_list_by_date"/></a><br>
<a href="${pageContext.request.contextPath}/orderListManager"><fmt:message key="manager_page.receipt_list_by_route"/></a><br>
<a href="${pageContext.request.contextPath}/LoadDeliveryManager"><fmt:message key="manager_page.delivery_list_by_directions"/></a><br>
<a href="${pageContext.request.contextPath}/manager/addnewdelivery.jsp"><fmt:message key="manager_page.add_new_delivery"/></a><br>
</body>
</html>