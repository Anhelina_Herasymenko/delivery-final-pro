package com.delivery.pro.servlets;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.delivery.pro.dao.ReceiptDAO;
import com.delivery.pro.entity.Receipt;

@WebServlet("/orderListManager")
public class LoadOrderListManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Receipt> receiptArray;
		HttpSession session = request.getSession();
		if(session.getAttribute("user") == null) {
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
		
		Map<Integer, String> receiptCategory = ReceiptDAO.getAllCategory();
		
        request.setAttribute("receiptCategory", receiptCategory);
        
        int receiptPerPage = 5;
        int receiptsByCategory = 1;
        
		if(request.getParameter("receiptCategoryId") != null) {
			receiptsByCategory = ReceiptDAO.getReceiptCount(Integer.parseInt(request.getParameter("receiptCategoryId")));
		}
		
		int numOfPages = (int) Math.ceil((double) receiptsByCategory/receiptPerPage);
		request.setAttribute("numOfPages", numOfPages);
		request.setAttribute("receiptPerPage", receiptPerPage);
		
		if(request.getParameter("receiptCategoryId") == null && request.getParameter("currentPage") == null) {
			request.setAttribute("receiptCategoryId", 1);
			request.setAttribute("currentPage", 1);
			receiptArray = ReceiptDAO.getAllReceipts(1, 0, receiptPerPage);
		} else if(request.getParameter("receiptCategoryId") != null && request.getParameter("currentPage") != null) {
			request.setAttribute("currentPage", request.getParameter("currentPage"));
			request.setAttribute("receiptCategoryId", request.getParameter("receiptCategoryId"));
			
			int currentPage = Integer.parseInt(request.getParameter("currentPage"));
			int receiptCategoryId = Integer.parseInt(request.getParameter("receiptCategoryId"));
			
			receiptArray = ReceiptDAO.getAllReceipts(receiptCategoryId, (currentPage-1)*receiptPerPage, receiptPerPage);
			
		} else {
			receiptArray = ReceiptDAO.getAllReceipts(1, 0, receiptPerPage);
		}
		request.setAttribute("receiptArray", receiptArray);
		request.getRequestDispatcher("manager/orderlistmanager.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}
