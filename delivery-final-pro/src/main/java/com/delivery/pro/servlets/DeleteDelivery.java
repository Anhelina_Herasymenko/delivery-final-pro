package com.delivery.pro.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.database.DBManager;

@WebServlet("/deleteDelivery")
public class DeleteDelivery extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(DeleteDelivery.class);
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		int deliveryId = Integer.parseInt(request.getParameter("deliveryId"));
		if(session.getAttribute("user") == null) {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		Connection conn = null;
		try {
			conn = DBManager.getInstance().getConnection();
			PreparedStatement ps = conn.prepareStatement("DELETE FROM delivery WHERE id=?;");
			ps.setInt(1, deliveryId);
			ps.executeUpdate();
			conn.close();
			ps.close();
		} catch (SQLException ex) {
			log.error("Cannot delete delivery with id " + deliveryId, ex);
			
		}
		response.sendRedirect("manager/result_of_deleting.jsp");
	}
}
