<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">

<head>
<title><fmt:message key="manager_page.user_list"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_manager2.jsp"/>
</head>
<body>
<div class="header" align="center">

<h1><fmt:message key="manager_page.user_list"/></h1>
<div class="info">
<a href="/delivery-final-pro/UserList"><fmt:message key="manager_page.user_list"/></a><br>
<h3>
    <table id="information" border="1">
        
         <tr><th>ID</th><td><c:out value="${user.id}" /></td></tr>
         <tr><th><fmt:message key="manager_page.users_firstname"/></th><td><c:out value="${user.firstName}" /></td></tr>
         <tr><th><fmt:message key="manager_page.users_lastname"/></th><td><c:out value="${user.lastName}" /></td></tr>
         <tr><th><fmt:message key="manager_page.users_email"/></th><td><c:out value="${user.email}" /></td></tr>
         <tr><th><fmt:message key="manager_page.users_create"/></th><td><c:out value="${user.createTime}" /></tr>
       </table>
</h3>
</div>
<br>
<a href="${pageContext.request.contextPath}/manager/manager_profile.jsp"><fmt:message key="manager_page.return_profile"/></a><br>

</div>
</body>
</html>