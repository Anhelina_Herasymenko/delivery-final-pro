<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/navigation_style.css">
</head>
<body>
<div class="navigation">
    <a href="${pageContext.request.contextPath}/user/user_profile.jsp"><fmt:message key="user_navigation.profile"/></a>
    <a href="${pageContext.request.contextPath}/LoadDeliveryUser"><fmt:message key="user_navigation.delivery_list"/></a>
    <a href="${pageContext.request.contextPath}/MyOrderInfo?userId=${user.id}"><fmt:message key="user_navigation.order_info"/></a>
    <a href="${pageContext.request.contextPath}/LogOut"><fmt:message key="user_navigation.log_out"/></a>
</div>
</body>
</html>