package com.delivery.pro.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.dao.ReceiptDAO;
import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.Receipt;
import com.delivery.pro.entity.User;

@WebServlet("/getUserOrderInfoByDatePagination")
public class UserOrderInfoByDatePagination extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(UserOrderInfoByDatePagination.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		
		int userId = Integer.parseInt(request.getParameter("userId"));
		System.out.println("userId = " + userId);
		if (session.getAttribute("user") == null) {
			request.getRequestDispatcher("login.jsp").forward(request, response);
		} else {
			Connection conn = null;
			User user = null;
			try {
				conn = DBManager.getInstance().getConnection();
				PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM user WHERE id=?;");
				pstmt.setInt(1, userId);
				ResultSet rs = pstmt.executeQuery();
				if (rs.next()) {
					user = User.createUser();
					user.setId(rs.getInt("id"));
				}
				conn.close();
				rs.close();
				pstmt.close();
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
			request.setAttribute("user", user);
		}
		
		List<Receipt> receiptList = new ArrayList<>();
		int receiptPerPage = 5;
		int allReceiptsOfUser = ReceiptDAO.getReceiptsOfUser();
		int numOfPages = (int) Math.ceil((double) allReceiptsOfUser/receiptPerPage);
		request.setAttribute("numOfPages", numOfPages);
		request.setAttribute("receiptPerPage", receiptPerPage);
		
		if (request.getParameter("createTimeFrom") == null && request.getParameter("createTimeTo") == null && request.getParameter("currentPage")==null) {
			request.setAttribute("currentPage", 1);
			receiptList = ReceiptDAO.AllOrdersOfUser(userId, 0, receiptPerPage);
		} else {
			String createTimeFrom = request.getParameter("createTimeFrom");
			System.out.println("createTimeFrom = " + createTimeFrom);
			String createTimeTo = request.getParameter("createTimeTo");
			System.out.println("createTimeTo = " + createTimeTo);
			int currentPage = Integer.parseInt(request.getParameter("currentPage"));
			receiptList = ReceiptDAO.AllOrdersOfUser(userId,createTimeFrom, createTimeTo, (currentPage-1)*receiptPerPage,receiptPerPage);
		}
			request.setAttribute("receipts", receiptList);
			System.out.println("receipts=" + receiptList);
			System.out.println("userIdAFTER = " + userId);
			request.getRequestDispatcher("/manager/user_order_list_by_date_pagination.jsp").forward(request, response);
	}
}