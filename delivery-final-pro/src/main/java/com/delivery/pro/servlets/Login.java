package com.delivery.pro.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.dao.InsertUserException;
import com.delivery.pro.dao.UserDAO;
import com.delivery.pro.entity.User;

@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(Login.class);
    
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		session.setMaxInactiveInterval(60*60*24);
		User user = (User) session.getAttribute("user");
		if (user == null) {
			try {
				user = UserDAO.userLogIn(request.getParameter("email"), request.getParameter("password"));
				session.setAttribute("user", user);
			} catch (InsertUserException iue) {
				log.error("Cannot insert user", iue);
				response.sendRedirect("error_pages/login_error.jsp");
			}
		}
		if(user != null && user.getRole() == User.ROLE_MANAGER) {
			response.sendRedirect("manager/manager_profile.jsp");
		} else if(user != null && user.getRole() == User.ROLE_USER) {
			response.sendRedirect("user/user_profile.jsp");
		}
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
}