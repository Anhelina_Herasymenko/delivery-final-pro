<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <title><fmt:message key="manager_page.order_list"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_manager.jsp"/>
</head>
<body>
<h2><fmt:message key="manager_page.order_list"/></h2>
    <h3><div class="navigation">
        <c:forEach items="${requestScope.receiptCategory}" var="category">
            <a href="${pageContext.request.contextPath}/orderListManager?receiptCategoryId=${category.key}&currentPage=1">${category.value}</a>
        </c:forEach>
    </div>
        <form action="${pageContext.request.contextPath}/orderListManager" method="get">
        <input type="hidden" value="${requestScope.receiptCategoryId}" name="receiptCategoryId">
        <input type="hidden" value="${requestScope.currentPage}" name="currentPage">
    </form>
</h3>    
    
<h2>
    <div class="info">
            <c:forEach var="i" begin="1" end="${requestScope.numOfPages}">
                <a href="${pageContext.request.contextPath}/orderListManager?receiptCategoryId=${requestScope.receiptCategoryId}&currentPage=${i}">${i}</a>
            </c:forEach>
    </div>
</h2>

 <div class="container">
    <table id="information" border="1">
    	<tr>
            <th>ID</th>
            <th><fmt:message key="manager_page.user_id"/></th>
            <th><fmt:message key="manager_page.user_email"/></th>
            <th><fmt:message key="manager_page.delivery_id"/></th>
            <th><fmt:message key="manager_page.delivery_route"/></th>
            <th><fmt:message key="manager_page.type"/></th>
            <th><fmt:message key="manager_page.weight"/></th>
            <th><fmt:message key="manager_page.size"/></th>
            <th><fmt:message key="manager_page.price"/></th>
            <th><fmt:message key="manager_page.create"/></th>
            <th><fmt:message key="manager_page.update"/></th>
            <th><fmt:message key="manager_page.pay_status_id"/></th>
            <th><fmt:message key="manager_page.pay_status"/></th>
            <th><fmt:message key="manager_page.status_id"/></th>
            <th><fmt:message key="manager_page.status"/></th>
            <th><fmt:message key="manager_page.category_id"/></th>
            <th><fmt:message key="manager_page.category_name"/></th>
            <th><fmt:message key="manager_page.get_info"/></th>
            <th><fmt:message key="manager_page.edit_info"/></th>
        </tr>
        <c:forEach var="receiptList" items="${requestScope.receiptArray}">
                <tr>
                <td><c:out value="${receiptList.id}" /></td>
            	<td><c:out value="${receiptList.user.id}" /></td>
            	<td><c:out value="${receiptList.user.email}" /></td>
            	<td><c:out value="${receiptList.delivery.id}" /></td>
           		<td><c:out value="${receiptList.delivery.route}" /></td>
            	<td><c:out value="${receiptList.type}" /></td>
            	<td><c:out value="${receiptList.weight}" /></td>
            	<td><c:out value="${receiptList.size}" /></td>
            	<td><c:out value="${receiptList.price}" /></td>
            	<td><c:out value="${receiptList.createTime}" /></td>
            	<td><c:out value="${receiptList.updateTime}" /></td>
            	<td><c:out value="${receiptList.payStatus}" /></td>
            	<td><c:out value="${receiptList.payStatusName}" /></td>
            	<td><c:out value="${receiptList.status}" /></td>
            	<td><c:out value="${receiptList.statusName}" /></td>
            	<td><c:out value="${receiptList.category}" /></td>
            	<td><c:out value="${receiptList.categoryName}" /></td>
            	<td><a href="getOrderInfo?receiptId=${receiptList.id}"><fmt:message key="manager_page.get_info"/></a></td>
            	<td><a href="editOrderInfo?receiptId=${receiptList.id}"><fmt:message key="manager_page.edit_info"/></a></td>
                </tr>
        </c:forEach>
        </table>
 </div>
 <a href="${pageContext.request.contextPath}/manager/manager_profile.jsp"><fmt:message key="manager_page.return_profile"/></a>
</body>
</html>