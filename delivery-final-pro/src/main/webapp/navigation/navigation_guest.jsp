<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/navigation_style.css">
</head>
<body>
<div class="navigation">
    <a href="${pageContext.request.contextPath}/index.jsp"><fmt:message key="index_page.home"/></a>
    <a href="${pageContext.request.contextPath}/loadDeliveryList"><fmt:message key="index_page.delivery_list"/></a>
	<a href="${pageContext.request.contextPath}/login.jsp"><fmt:message key="index_page.login"/></a>
	<a href="${pageContext.request.contextPath}/register.jsp"><fmt:message key="index_page.register"/></a>
	<a href="?sessionLocale=en"><fmt:message key="user_navigation.locale_en"/></a>
	<a href="?sessionLocale=uk"><fmt:message key="user_navigation.locale_uk"/></a>

</div>
</body>
</html>