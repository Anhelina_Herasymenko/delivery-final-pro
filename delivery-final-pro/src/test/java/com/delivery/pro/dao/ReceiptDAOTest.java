package com.delivery.pro.dao;

import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.Delivery;
import com.delivery.pro.entity.Receipt;
import com.delivery.pro.entity.User;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ReceiptDAOTest {
	DBManager dbManager;
	String deliveryRouteTest = "test MIDSIZE 5...10";
	String userEmailTest = "test001@gmail.com";
	User user = null;
	Delivery delivery = null;
	@BeforeEach
	public void getConnection() {
		dbManager = DBManager.getInstance(true);
	}
	
	@BeforeAll
	public static void getConnectionBefore() {
		DBManager dbManager = DBManager.getInstance();
	}
	
	@Test
	public void shouldReturnAllOrders() {
		List<Receipt> receiptListTest = ReceiptDAO.getAllOrders();
		
		
	}
	
	
	@BeforeEach
	public void beforeTest() {
		User user = User.createUser();
		user.setEmail("test001@gmail.com");
		user.setFirstName("Test001");
		user.setLastName("Test");
		user.setPassword("test");
		UserDAO.insertUser(user);
		
		Delivery testDelivery = Delivery.createDelivery();
		testDelivery.setRoute("test MIDSIZE 5...10");
		testDelivery.setWeightLimit("5...10");
		testDelivery.setSize("MIDSIZE");
		testDelivery.setTarif(42.50);
		testDelivery.setDescription("test");
		testDelivery.setCategory(1);
		DeliveryDAO.addDeliveryToDB(testDelivery);
	}
	
	@AfterEach
	public void afterTest() {
		Delivery delivery = Delivery.createDelivery();
		delivery.setRoute("test MIDSIZE 5...10");
		DeliveryDAO.deleteDelivery(delivery);
		UserDAO.deleteUserByEmail("test001@gmail.com");
	}

}
