<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <title><fmt:message key="index_page.title"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
    <jsp:include page="/navigation/navigation_guest.jsp"/>
</head>
<body>
<h4>
<fmt:message key="guest_page.for_delivery"/> <a href="login.jsp"><fmt:message key="guest_page.for_delivery_login"/></a>
<fmt:message key="index_page.or"/> <a href="register.jsp"><fmt:message key="guest_page.for_delivery_register"/></a><br></h4>
<h2><fmt:message key="guest_page.delivery"/></h2>
    <h3><div class="navigation">
        <c:forEach items="${requestScope.deliveryCategory}" var="category">
            <a href="${pageContext.request.contextPath}/loadDeliveryList?deliveryCategoryId=${category.key}&deliverySort=${requestScope.deliverySort}&currentPage=1">${category.value}</a>
        </c:forEach>
    </div>
        <form action="${pageContext.request.contextPath}/loadDeliveryList" method="get">
        <input type="hidden" value="sort_select">
        <input type="hidden" value="${requestScope.deliveryCategoryId}" name="deliveryCategoryId">
<%--         <input type="hidden" value="${requestScope.currentPage}" name="currentPage"> --%>
    </form>
</h3>    
    
<h2>
    <div class="info">
            <c:forEach var="i" begin="1" end="${requestScope.numOfPages}">
                <a href="${pageContext.request.contextPath}/loadDeliveryList?deliveryCategoryId=${requestScope.deliveryCategoryId}&deliverySort=${requestScope.deliverySort}&currentPage=${i}">${i}</a>
            </c:forEach>
    </div>
</h2>

 <div class="container">
    <table id="information" border="1">
    	<tr>
            <th><fmt:message key="guest_page.route"/></th>
            <th><fmt:message key="guest_page.weightlimit"/></th>
            <th><fmt:message key="guest_page.size"/></th>
            <th><fmt:message key="guest_page.tarif"/></th>
            <th><fmt:message key="guest_page.description"/></th>
        </tr>
        <c:forEach var="deliveries" items="${requestScope.deliveryArray}">
                <tr>
                <td><c:out value="${deliveries.route}" /></td>
                <td><c:out value="${deliveries.weightLimit}" /></td>
                <td><c:out value="${deliveries.size}" /></td>
                <td><c:out value="${deliveries.tarif}" /></td>
                <td><c:out value="${deliveries.description}" /></td>
                </tr>
        </c:forEach>
        </table>
 </div>
</body>
</html>