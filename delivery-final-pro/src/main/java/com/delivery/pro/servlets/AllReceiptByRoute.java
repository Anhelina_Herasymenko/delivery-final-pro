package com.delivery.pro.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.delivery.pro.dao.ReceiptDAO;
import com.delivery.pro.entity.Receipt;

@WebServlet("/AllReceiptRoute")
public class AllReceiptByRoute extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		List<Receipt> receiptList = ReceiptDAO.getAllOrdersByRoute();
		request.setAttribute("receipts", receiptList);
		System.out.println("receipts = " + receiptList);
		if(session.getAttribute("user") == null) {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		request.getRequestDispatcher("/manager/orderlistbyroute.jsp").forward(request, response);
		
	}

}