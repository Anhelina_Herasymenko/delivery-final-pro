<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">

<head>
<title><fmt:message key="manager_page.order_info"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
</head>
<body>
<div class="header" align="center">

<h1><fmt:message key="manager_page.order_list"/></h1>
<div class="info">
<h3>
    <table id="information" border="1">
    	<tr><th>ID<td><c:out value="${receipt.id}" /></td></tr>
    	<tr><th><fmt:message key="manager_page.user_id"/></th><td><c:out value="${receipt.user.id}" /></td></tr>
    	<tr><th><fmt:message key="manager_page.user_email"/></th><td><c:out value="${receipt.user.email}" /></td></tr>
    	<tr><th><fmt:message key="manager_page.delivery_id"/></th><td><c:out value="${receipt.delivery.id}" /></td></tr>
    	<tr><th><fmt:message key="manager_page.delivery_route"/></th><td><c:out value="${receipt.delivery.route}" /></td></tr>
    	<tr><th><fmt:message key="manager_page.type"/></th><td><c:out value="${receipt.type}" /></td></tr>
    	<tr><th><fmt:message key="manager_page.weight"/></th><td><c:out value="${receipt.weight}" /></td></tr>
    	<tr><th><fmt:message key="manager_page.size"/></th><td><c:out value="${receipt.size}" /></td></tr>
    	<tr><th><fmt:message key="manager_page.price"/></th><td><c:out value="${receipt.price}" /></td></tr>
    	<tr><th><fmt:message key="manager_page.create"/></th><td><c:out value="${receipt.createTime}" /></td></tr>
    	<tr><th><fmt:message key="manager_page.update"/></th><td><c:out value="${receipt.updateTime}" /></td></tr>
    	<tr><th><fmt:message key="manager_page.pay_status"/></th><td><c:out value="${receipt.payStatusName}" /></td></tr>
    	<tr><th><fmt:message key="manager_page.status"/></th><td><c:out value="${receipt.statusName}" /></td></tr>
	</table>
</h3>
</div>
<br>
<a href="javascript:window.print()"><fmt:message key="manager_page.print_record"/></a><br>
</div>
</body>
</html>