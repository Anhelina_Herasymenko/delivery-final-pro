package com.delivery.pro.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.delivery.pro.dao.UserDAO;
import com.delivery.pro.entity.User;

@WebServlet("/UserList")
public class UserList extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		List<User> userArray;	
		if(session.getAttribute("user") == null) {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		
		int allUsers = UserDAO.getUserCount();
		int userPerPage = 5;
		int numOfPages = (int) Math.ceil((double) allUsers/userPerPage);
		request.setAttribute("numOfPages", numOfPages);
		request.setAttribute("userPerPage", userPerPage);
		
		if(request.getParameter("currentPage") == null) {
			request.setAttribute("currentPage", 1);
			userArray = UserDAO.getAllUsersManager(0, userPerPage);
		} else {
			request.setAttribute("currentPage", request.getParameter("currentPage"));
			int currentPage = Integer.parseInt(request.getParameter("currentPage"));
			userArray = UserDAO.getAllUsersManager((currentPage-1)*userPerPage, userPerPage);
		} 
		request.setAttribute("userArray", userArray);
		request.getRequestDispatcher("/manager/user_list.jsp").forward(request, response);
	}
}