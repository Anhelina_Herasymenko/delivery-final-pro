<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">
<head>
<title><fmt:message key="registration_error"/></title>
</head>
<body>
<h3><fmt:message key="registration_error"/></h3><br>
<a href="${pageContext.request.contextPath}/index.jsp"><fmt:message key="index_page.home"/></a>
</body>
</html>