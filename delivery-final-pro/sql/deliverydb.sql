-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema deliverydb
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `deliverydb` ;

-- -----------------------------------------------------
-- Schema deliverydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `deliverydb` DEFAULT CHARACTER SET utf8 ;
USE `deliverydb` ;

-- -----------------------------------------------------
-- Table `deliverydb`.`user_role`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverydb`.`user_role` ;

CREATE TABLE IF NOT EXISTS `deliverydb`.`user_role` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` ENUM("ADMIN", "USER", "GUEST") NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `deliverydb`.`user`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverydb`.`user` ;

CREATE TABLE IF NOT EXISTS `deliverydb`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `firstname` VARCHAR(255) NOT NULL,
  `lastname` VARCHAR(255) NOT NULL,
  `email` VARCHAR(255) NOT NULL,
  `password` VARCHAR(255) NOT NULL,
  `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `user_role_id` INT NOT NULL DEFAULT 2,
  PRIMARY KEY (`id`),
  INDEX `fk_user_user_role_idx` (`user_role_id` ASC) VISIBLE,
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE,
  CONSTRAINT `fk_user_user_role`
    FOREIGN KEY (`user_role_id`)
    REFERENCES `deliverydb`.`user_role` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `deliverydb`.`category`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverydb`.`category` ;

CREATE TABLE IF NOT EXISTS `deliverydb`.`category` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `deliverydb`.`delivery`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverydb`.`delivery` ;

CREATE TABLE IF NOT EXISTS `deliverydb`.`delivery` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `route` VARCHAR(1024) NOT NULL,
  `weight_limit` VARCHAR(255) NOT NULL,
  `size` VARCHAR(255) NOT NULL,
  `tarif` DOUBLE UNSIGNED NOT NULL,
  `description` VARCHAR(1024) NULL,
  `category_id` INT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_delivery_category1_idx` (`category_id` ASC) VISIBLE,
  CONSTRAINT `fk_delivery_category1`
    FOREIGN KEY (`category_id`)
    REFERENCES `deliverydb`.`category` (`id`)
    ON DELETE SET NULL
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `deliverydb`.`pay_status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverydb`.`pay_status` ;

CREATE TABLE IF NOT EXISTS `deliverydb`.`pay_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` ENUM("UNPAID", "PAID") NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `deliverydb`.`status`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverydb`.`status` ;

CREATE TABLE IF NOT EXISTS `deliverydb`.`status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` ENUM("NEW", "IN_PROGRESS", "DONE", "CANCELLED") NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `name_UNIQUE` (`name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `deliverydb`.`receipt`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `deliverydb`.`receipt` ;

CREATE TABLE IF NOT EXISTS `deliverydb`.`receipt` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `delivery_id` INT NOT NULL,
  `type` VARCHAR(255) NOT NULL,
  `weight` DOUBLE UNSIGNED NOT NULL,
  `size` VARCHAR(255) NOT NULL,
  `price` DOUBLE UNSIGNED NOT NULL DEFAULT 0.0,
  `create_time` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP(),
  `update_time` DATETIME NOT NULL DEFAULT  CURRENT_TIMESTAMP() ON UPDATE CURRENT_TIMESTAMP(),
  `pay_status_id` INT NOT NULL DEFAULT 1,
  `status_id` INT NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`),
  INDEX `fk_receipt_pay_status_idx` (`pay_status_id` ASC) VISIBLE,
  INDEX `fk_receipt_status_idx` (`status_id` ASC) VISIBLE,
  INDEX `fk_receipt_user_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_receipt_delivery_idx` (`delivery_id` ASC) INVISIBLE,
  CONSTRAINT `fk_receipt_pay_status`
    FOREIGN KEY (`pay_status_id`)
    REFERENCES `deliverydb`.`pay_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_receipt_status`
    FOREIGN KEY (`status_id`)
    REFERENCES `deliverydb`.`status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE,
  CONSTRAINT `fk_receipt_user`
    FOREIGN KEY (`user_id`)
    REFERENCES `deliverydb`.`user` (`id`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_receipt_delivery`
    FOREIGN KEY (`delivery_id`)
    REFERENCES `deliverydb`.`delivery` (`id`)
    ON DELETE NO ACTION
    ON UPDATE CASCADE)
ENGINE = InnoDB;

USE `deliverydb`;

DELIMITER $$

USE `deliverydb`$$
DROP TRIGGER IF EXISTS `deliverydb`.`receipt_BEFORE_INSERT` $$
USE `deliverydb`$$
CREATE DEFINER = CURRENT_USER TRIGGER `deliverydb`.`receipt_BEFORE_INSERT` BEFORE INSERT ON `receipt` FOR EACH ROW
BEGIN
SET NEW.price = (select tarif from delivery where id = NEW.delivery_id);
END$$


DELIMITER ;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `deliverydb`.`user_role`
-- -----------------------------------------------------
START TRANSACTION;
USE `deliverydb`;
INSERT INTO `deliverydb`.`user_role` (`id`, `name`) VALUES (1, 'ADMIN');
INSERT INTO `deliverydb`.`user_role` (`id`, `name`) VALUES (2, 'USER');
INSERT INTO `deliverydb`.`user_role` (`id`, `name`) VALUES (3, 'GUEST');

COMMIT;


-- -----------------------------------------------------
-- Data for table `deliverydb`.`user`
-- -----------------------------------------------------
START TRANSACTION;
USE `deliverydb`;
INSERT INTO `deliverydb`.`user` (`id`, `firstname`, `lastname`, `email`, `password`, `create_time`, `user_role_id`) VALUES (1, 'admin', 'admin', 'admin@gmail.com', '$2a$12$ehoudtlwfzljDf4z7P.d3Oxyxhm1O.X6VSPHEbL8T/Pk/Or/sPDbC', '2020-01-01', 1);
INSERT INTO `deliverydb`.`user` (`id`, `firstname`, `lastname`, `email`, `password`, `create_time`, `user_role_id`) VALUES (2, 'Anna', 'Petrenko', '98492@gmail.com', '$2a$12$ehoudtlwfzljDf4z7P.d3Oxyxhm1O.X6VSPHEbL8T/Pk/Or/sPDbC', '2020-02-01', 2);
INSERT INTO `deliverydb`.`user` (`id`, `firstname`, `lastname`, `email`, `password`, `create_time`, `user_role_id`) VALUES (3, 'Petr', 'Ivanchenko', 'wfe74f8@gmail.com', '$2a$12$ehoudtlwfzljDf4z7P.d3Oxyxhm1O.X6VSPHEbL8T/Pk/Or/sPDbC', '2020-07-25', 2);
INSERT INTO `deliverydb`.`user` (`id`, `firstname`, `lastname`, `email`, `password`, `create_time`, `user_role_id`) VALUES (4, 'Sergei', 'Galushko', 'ewg824r9868we@gmail.com', '$2a$12$ehoudtlwfzljDf4z7P.d3Oxyxhm1O.X6VSPHEbL8T/Pk/Or/sPDbC', '2020-07-30', 2);
INSERT INTO `deliverydb`.`user` (`id`, `firstname`, `lastname`, `email`, `password`, `create_time`, `user_role_id`) VALUES (5, 'Svetlana', 'Petrova', 'w8924ef4@gmail.com', '$2a$12$ehoudtlwfzljDf4z7P.d3Oxyxhm1O.X6VSPHEbL8T/Pk/Or/sPDbC', '2020-08-08', 2);
INSERT INTO `deliverydb`.`user` (`id`, `firstname`, `lastname`, `email`, `password`, `create_time`, `user_role_id`) VALUES (6, 'Aleksandr', 'Ivanov', 'he427tr628fr@gmail.com', '$2a$12$ehoudtlwfzljDf4z7P.d3Oxyxhm1O.X6VSPHEbL8T/Pk/Or/sPDbC', '2020-11-10', 2);
INSERT INTO `deliverydb`.`user` (`id`, `firstname`, `lastname`, `email`, `password`, `create_time`, `user_role_id`) VALUES (7, 'Olga', 'Sergeenko', '894rg8@gmail.com', '$2a$12$ehoudtlwfzljDf4z7P.d3Oxyxhm1O.X6VSPHEbL8T/Pk/Or/sPDbC', '2020-12-24', 2);
INSERT INTO `deliverydb`.`user` (`id`, `firstname`, `lastname`, `email`, `password`, `create_time`, `user_role_id`) VALUES (8, 'Yuriy', 'Andreev', 'egw724978@gmail.com', '$2a$12$ehoudtlwfzljDf4z7P.d3Oxyxhm1O.X6VSPHEbL8T/Pk/Or/sPDbC', '2021-01-01', 2);
INSERT INTO `deliverydb`.`user` (`id`, `firstname`, `lastname`, `email`, `password`, `create_time`, `user_role_id`) VALUES (9, 'Kateryna', 'Andreeva', 'rg874289gr@gmail.com', '$2a$12$ehoudtlwfzljDf4z7P.d3Oxyxhm1O.X6VSPHEbL8T/Pk/Or/sPDbC', '2021-03-16', 2);
INSERT INTO `deliverydb`.`user` (`id`, `firstname`, `lastname`, `email`, `password`, `create_time`, `user_role_id`) VALUES (10, 'Oleg', 'Petrov', 'rg84892gr@gmail.com', '$2a$12$ehoudtlwfzljDf4z7P.d3Oxyxhm1O.X6VSPHEbL8T/Pk/Or/sPDbC', '2021-04-14', 2);
INSERT INTO `deliverydb`.`user` (`id`, `firstname`, `lastname`, `email`, `password`, `create_time`, `user_role_id`) VALUES (11, 'User', 'User', 'aaa@gmail.com', '$2a$12$ehoudtlwfzljDf4z7P.d3Oxyxhm1O.X6VSPHEbL8T/Pk/Or/sPDbC', '2021-06-02 19:02:07', 2);
INSERT INTO `deliverydb`.`user` (`id`, `firstname`, `lastname`, `email`, `password`, `create_time`, `user_role_id`) VALUES (12, 'Test001', 'Test', 'test@gmail.com', '$2a$12$ehoudtlwfzljDf4z7P.d3Oxyxhm1O.X6VSPHEbL8T/Pk/Or/sPDbC', '2021-06-03 19:02:07', 2);
COMMIT;


-- -----------------------------------------------------
-- Data for table `deliverydb`.`category`
-- -----------------------------------------------------
START TRANSACTION;
USE `deliverydb`;
INSERT INTO `deliverydb`.`category` (`id`, `name`) VALUES (1, 'From Kyiv');
INSERT INTO `deliverydb`.`category` (`id`, `name`) VALUES (2, 'From Lviv');
INSERT INTO `deliverydb`.`category` (`id`, `name`) VALUES (3, 'From Dnipro');
INSERT INTO `deliverydb`.`category` (`id`, `name`) VALUES (4, 'From Kharkiv');
INSERT INTO `deliverydb`.`category` (`id`, `name`) VALUES (5, 'From Odessa');

COMMIT;


-- -----------------------------------------------------
-- Data for table `deliverydb`.`delivery`
-- -----------------------------------------------------
START TRANSACTION;
USE `deliverydb`;
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kyiv-Dnipro (Smallsize 0-1 kg)', '0...1', 'SMALLSIZE', 30.00, NULL, 1);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kyiv-Dnipro (Smallsize 1-5 kg)', '1...5', 'SMALLSIZE', 40.00, NULL, 1);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kyiv-Dnipro (Midsize 5-10 kg)', '5...10', 'MIDSIZE', 50.00, NULL, 1);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kyiv-Dnipro (Midsize 10-20 kg)', '10...20', 'MIDSIZE', 70.00, NULL, 1);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kyiv-Dnipro (Largesize 20-50 kg)', '20...50', 'LARGESIZE', 100.00, NULL, 1);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kyiv-Dnipro (Largesize 50-100 kg)', '50...100', 'LARGESIZE', 150.00, NULL, 1);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kyiv-Lviv (Smallsize 0-1 kg)', '0...1', 'SMALLSIZE', 32.00, NULL, 1);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kyiv-Lviv (Smallsize 1-5 kg)', '1...5', 'SMALLSIZE', 43.00, NULL, 1);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kyiv-Lviv (Midsize 5-10 kg)', '5...10', 'MIDSIZE', 55.00, NULL, 1);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kyiv-Lviv (Midsize 10-20 kg)', '10...20', 'MIDSIZE', 78.00, NULL, 1);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kyiv-Lviv (Largesize 20-50 kg)', '20...50', 'LARGESIZE', 110.00, NULL, 1);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kyiv-Lviv (Largesize 50-100 kg)', '50...100', 'LARGESIZE', 165.00, NULL, 1);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Lviv-Dnipro (Smallsize 0-1 kg)', '0...1', 'SMALLSIZE', 40.00, NULL, 2);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Lviv-Dnipro (Smallsize 1-5 kg)', '1...5', 'SMALLSIZE', 50.00, '', 2);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Lviv-Dnipro (Midsize 5-10 kg)', '5...10', 'MIDSIZE', 60.00, NULL, 2);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Lviv-Dnipro (Midsize 10-20 kg)', '10...20', 'MIDSIZE', 80.00, NULL, 2);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Lviv-Dnipro (Largesize 20-50 kg)', '20...50', 'LARGESIZE', 115.00, NULL, 2);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Lviv-Dnipro (Largesize 50-100 kg)', '50...100', 'LARGESIZE', 175.00, NULL, 2);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Dnipro-Kyiv (Smallsize 0-1 kg)', '0...1', 'SMALLSIZE', 30.00, NULL, 3);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Dnipro-Kyiv (Smallsize 1-5 kg)', '1...5', 'SMALLSIZE', 40.00, NULL, 3);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Dnipro-Kyiv (Midsize 5-10 kg)', '5...10', 'MIDSIZE', 50.00, NULL, 3);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Dnipro-Kyiv (Midsize 10-20 kg)', '10...20', 'MIDSIZE', 70.00, NULL, 3);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Dnipro-Kyiv (Largesize 20-50 kg)', '20...50', 'LARGESIZE', 100.00, NULL, 3);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Dnipro-Kyiv (Largesize 50-100 kg)', '50...100', 'LARGESIZE', 150.00, NULL, 3);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kharkiv-Kyiv (Smallsize 0-1 kg)', '0...1', 'SMALLSIZE', 31.00, NULL, 4);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kharkiv-Kyiv (Smallsize 1-5 kg)', '1...5', 'SMALLSIZE', 42.00, NULL, 4);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kharkiv-Kyiv (Midsize 5-10 kg)', '5...10', 'MIDSIZE', 54.00, NULL, 4);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kharkiv-Kyiv (Midsize 10-20 kg)', '10...20', 'MIDSIZE', 75.00, NULL, 4);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kharkiv-Kyiv (Largesize 20-50 kg)', '20...50', 'LARGESIZE', 110.00, NULL, 4);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Kharkiv-Kyiv (Largesize 50-100 kg)', '50...100', 'LARGESIZE', 160.00, NULL, 4);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Odessa-Kyiv (Smallsize 0-1 kg)', '0...1', 'SMALLSIZE', 35.00, NULL, 5);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Odessa-Kyiv (Smallsize 1-5 kg)', '1...5', 'SMALLSIZE', 46.00, NULL, 5);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Odessa-Kyiv (Midsize 5-10 kg)', '5...10', 'MIDSIZE', 58.00, NULL, 5);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Odessa-Kyiv (Midsize 10-20 kg)', '10...20', 'MIDSIZE', 80.00, NULL, 5);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Odessa-Kyiv (Largesize 20-50 kg)', '20...50', 'LARGESIZE', 120.00, NULL, 5);
INSERT INTO `deliverydb`.`delivery` (`id`, `route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) VALUES (DEFAULT, 'Odessa-Kyiv (Largesize 50-100 kg)', '50...100', 'LARGESIZE', 170.00, NULL, 5);

COMMIT;


-- -----------------------------------------------------
-- Data for table `deliverydb`.`pay_status`
-- -----------------------------------------------------
START TRANSACTION;
USE `deliverydb`;
INSERT INTO `deliverydb`.`pay_status` (`id`, `name`) VALUES (1, 'UNPAID');
INSERT INTO `deliverydb`.`pay_status` (`id`, `name`) VALUES (2, 'PAID');

COMMIT;


-- -----------------------------------------------------
-- Data for table `deliverydb`.`status`
-- -----------------------------------------------------
START TRANSACTION;
USE `deliverydb`;
INSERT INTO `deliverydb`.`status` (`id`, `name`) VALUES (1, 'NEW');
INSERT INTO `deliverydb`.`status` (`id`, `name`) VALUES (2, 'IN_PROGRESS');
INSERT INTO `deliverydb`.`status` (`id`, `name`) VALUES (3, 'DONE');
INSERT INTO `deliverydb`.`status` (`id`, `name`) VALUES (4, 'CANCELLED');

COMMIT;


-- -----------------------------------------------------
-- Data for table `deliverydb`.`receipt`
-- -----------------------------------------------------
START TRANSACTION;
USE `deliverydb`;
INSERT INTO `deliverydb`.`receipt` (`id`, `user_id`, `delivery_id`, `type`, `weight`, `size`, `price`, `create_time`, `update_time`, `pay_status_id`, `status_id`) VALUES (1, 2, 4, 'delivery', 11.50, 'MIDSIZE', 70.00, '2020-12-28', '2020-12-30', 2, 3);
INSERT INTO `deliverydb`.`receipt` (`id`, `user_id`, `delivery_id`, `type`, `weight`, `size`, `price`, `create_time`, `update_time`, `pay_status_id`, `status_id`) VALUES (2, 3, 8, 'other', 2.00, 'SMALLSIZE', 43.00, '2021-01-01', '2021-01-02', 2, 3);
INSERT INTO `deliverydb`.`receipt` (`id`, `user_id`, `delivery_id`, `type`, `weight`, `size`, `price`, `create_time`, `update_time`, `pay_status_id`, `status_id`) VALUES (3, 4, 11, 'other', 25.00, 'LARGESIZE', 110.00, '2021-01-15', '2021-01-17', 2, 3);
INSERT INTO `deliverydb`.`receipt` (`id`, `user_id`, `delivery_id`, `type`, `weight`, `size`, `price`, `create_time`, `update_time`, `pay_status_id`, `status_id`) VALUES (4, 4, 4, 'other', 12.00, 'MIDSIZE', 70.00, '2021-01-16', '2021-01-18', 2, 2);
INSERT INTO `deliverydb`.`receipt` (`id`, `user_id`, `delivery_id`, `type`, `weight`, `size`, `price`, `create_time`, `update_time`, `pay_status_id`, `status_id`) VALUES (5, 3, 8, 'other', 2.00, 'SMALLSIZE', 43.00, '2021-01-17', '2021-01-19', 2, 3);
INSERT INTO `deliverydb`.`receipt` (`id`, `user_id`, `delivery_id`, `type`, `weight`, `size`, `price`, `create_time`, `update_time`, `pay_status_id`, `status_id`) VALUES (6, 2, 11, 'other', 26.00, 'LARGESIZE', 110.00, '2021-01-18', '2021-01-20', 1, 4);
INSERT INTO `deliverydb`.`receipt` (`id`, `user_id`, `delivery_id`, `type`, `weight`, `size`, `price`, `create_time`, `update_time`, `pay_status_id`, `status_id`) VALUES (7, 4, 11, 'other', 27.00, 'LARGESIZE', 110.00, '2021-01-19', '2021-01-21', 2, 3);

COMMIT;

