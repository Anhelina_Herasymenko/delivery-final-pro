<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">
<head>
<title><fmt:message key="manager_page.users_edit"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_manager2.jsp"/>
</head>
<body>
<div class="header" align="center">
<h2><fmt:message key="manager_page.user_info"/></h2>
<div class="info">
<a href="/delivery-final-pro/UserList"><fmt:message key="manager_page.user_list"/></a><br>
<h3>
	<form action="updateUser" method="post"><table>
		<tr><td>ID</td><th><input name="userId" value="${editUser.id}" readonly/><th/></tr><br>
		<tr><td><fmt:message key="manager_page.users_firstname"/></td><th><input name="firstName" value="${editUser.firstName}" /><th/></tr>
		<tr><td><fmt:message key="manager_page.users_firstname"/></td><th><input name="lastName" value="${editUser.lastName}" /><th/></tr>
		<tr><td><fmt:message key="manager_page.users_email"/></td><th><input name="email" value="${editUser.email}" /><th/></tr>
		<tr><td><input type="submit" value="Save" /><td/><tr/>
	</table></form>
<br>
</h3>
</div>
<br>
<a href="${pageContext.request.contextPath}/manager/manager_profile.jsp"><fmt:message key="manager_page.return_profile"/></a><br>
</div>
</body>
</html>