package com.delivery.pro.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.dao.ReceiptDAO;
import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.Delivery;
import com.delivery.pro.entity.Receipt;
import com.delivery.pro.entity.User;

@WebServlet("/makeOrder")
public class MakeOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(MakeOrder.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Delivery delivery = null;
		HttpSession session = request.getSession();
		if (session.getAttribute("user") == null) {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		int deliveryId = Integer.parseInt(request.getParameter("deliveryId"));

		Connection conn = null;
		try {
			conn = DBManager.getInstance().getConnection();
			PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM delivery WHERE id=?");
			pstmt.setInt(1, deliveryId);

			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				delivery = Delivery.createDelivery();
				delivery = new Delivery();
				delivery.setId(rs.getInt("id"));
				delivery.setRoute(rs.getString("route"));
				delivery.setTarif(rs.getDouble("tarif"));
			}
			conn.close();
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			log.error("Cannot find delivery info", ex);
		}
		request.setAttribute("delivery", delivery);
		request.getRequestDispatcher("/user/makeorder.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		User user = (User) request.getSession().getAttribute("user");
		int userId = Integer.parseInt(request.getParameter("userId"));
		Delivery delivery = null;
		int deliveryId = Integer.parseInt(request.getParameter("deliveryId"));

		Connection conn = null;
		try {
			conn = DBManager.getInstance().getConnection();
			PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM delivery WHERE id=?");
			pstmt.setInt(1, deliveryId);

			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				delivery = Delivery.createDelivery();
				delivery = new Delivery();
				delivery.setId(rs.getInt("id"));
				delivery.setRoute(rs.getString("route"));
				delivery.setTarif(rs.getDouble("tarif"));
			}
			conn.close();
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			log.error("Cannot find delivery info", ex);
		}
		request.setAttribute("delivery", delivery);

		Receipt receipt = null;
		try {
			conn = DBManager.getInstance().getConnection();
			PreparedStatement pstmt = conn.prepareStatement(
					"INSERT INTO receipt "
							+ "(`user_id`, `delivery_id`, `type`, `weight`, `size`) VALUES (?, ?, ?, ?, ?);",
					Statement.RETURN_GENERATED_KEYS);
			pstmt.setInt(1, userId);
			pstmt.setInt(2, deliveryId);
			if (receipt == null) {
				receipt = Receipt.createReceipt();
				user.setId(userId);
				delivery.setId(deliveryId);
				receipt.setType(request.getParameter("type"));
				receipt.setWeight(Double.parseDouble(request.getParameter("weight")));
				receipt.setSize(request.getParameter("size"));
				session.setAttribute("receipt", receipt);
			}
			ReceiptDAO.insertReceipt(receipt, userId, deliveryId);
		} catch (SQLException ex) {
			log.error("Cannot make an order", ex);
		}
		log.info("Order " + receipt.getId() + " has been saved to database");
		response.sendRedirect("user/result_order.jsp");
	}
}
