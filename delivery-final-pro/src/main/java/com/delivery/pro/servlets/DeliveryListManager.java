package com.delivery.pro.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.delivery.pro.dao.DeliveryDAO;
import com.delivery.pro.entity.Delivery;

@WebServlet("/DeliveryListManager")
public class DeliveryListManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user") == null) {
			request.getRequestDispatcher("/DeliveryList").forward(request, response);
		}
		List<Delivery> deliveryList = DeliveryDAO.selectAllDelivery();
		request.setAttribute("deliveries", deliveryList);
		System.out.println("deliveries = " + deliveryList);
		request.getRequestDispatcher("manager/deliverylistmanager.jsp").forward(request, response);
		
	}

}
