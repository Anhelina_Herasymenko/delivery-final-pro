<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">

<head>
<title><fmt:message key="user_page.order.title"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_user2.jsp"/>
</head>
<body class="text-center">
<main class="form-add_order">
<form action="${pageContext.request.contextPath}/makeOrder" method="post">
    <div class="info">
        <h2><fmt:message key="user_page.order.title"/></h2><br>
    <table id="information" border="1">
    <tr><th><fmt:message key="user_page.deliveryID"/><td><input name="deliveryId" value="${delivery.id}" readonly /></td></tr>
    <tr><th><fmt:message key="user_page.route"/><td><c:out value="${delivery.route}" /></td></tr>
    <tr><th><fmt:message key="user_page.tarif"/><td><c:out value="${delivery.tarif}" /></td></tr>
    <tr><th><fmt:message key="user_page.userID"/><td><input name="userId" value="${user.id}" readonly/></td></tr>
    </table>
    <p><fmt:message key="user_page.order_fields"/></p><br>
    <b><fmt:message key="user_page.insert_info_type"/></b><br>
	<select name="type" required>
            <option><fmt:message key="user_page.package"/></option>
            <option><fmt:message key="user_page.documents"/></option>
            <option><fmt:message key="user_page.fragile"/></option>
            <option><fmt:message key="user_page.other"/></option>
    </select><br>
    
    <b><fmt:message key="user_page.insert_info_weight"/></b><br>
        <input pattern="(\d*\.?\d+)\s?" type="text" name="weight" placeholder="5.5" required/><br>
        
    <b><fmt:message key="user_page.insert_info_size"/></b><br>
    <select name="size" required>
            <option>SMALLSIZE</option>
            <option>MIDSIZE</option>
            <option>LARGESIZE</option>
    </select><br>

    <input type="submit" value="<fmt:message key="user_page.order.submit_order"/>"/>
	<input type="button" value="<fmt:message key="user_page.cancel"/>" onClick="javascript:window.location='${pageContext.request.contextPath}/user/user_profile.jsp';"><br>
            <input type="hidden" name="add_order_form">
    </div>
</form>
<a href="${pageContext.request.contextPath}/user/user_profile.jsp"><fmt:message key="user_page.return_profile"/></a>
</main>
</body>
</html>