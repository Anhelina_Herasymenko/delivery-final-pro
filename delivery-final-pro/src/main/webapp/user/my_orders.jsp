<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">
<head>
<title><fmt:message key="user_navigation.order_info"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_user2.jsp"/>
</head>
<body>
<h3>
    <table id="information" border="1">
        <tr>
            <th>ID</th>
			<th><fmt:message key="user_page.userID"/></th>
            <th><fmt:message key="user_page.user_email"/></th>
            <th><fmt:message key="user_page.deliveryID"/></th> 
            <th><fmt:message key="user_page.route"/></th>
            <th><fmt:message key="user_page.type"/></th>
            <th><fmt:message key="user_page.weight"/></th>
            <th><fmt:message key="user_page.size"/></th>
            <th><fmt:message key="user_page.price"/></th>
            <th><fmt:message key="user_page.create"/></th>
            <th><fmt:message key="user_page.update"/></th>
            <th><fmt:message key="user_page.pay_status"/></th>
            <th><fmt:message key="user_page.status"/></th>
            <th><fmt:message key="user_page.cart"/></th>
            <th><fmt:message key="user_page.order.cancel_order"/></th>
        </tr>
        <c:forEach items="${receipts}" var="receiptList" >
            <tr>
            <td><c:out value="${receiptList.id}" /></td>
            <td><c:out value="${receiptList.user.id}" /></td>
            <td><c:out value="${receiptList.user.email}" /></td>
            <td><c:out value="${receiptList.delivery.id}" /></td>
            <td><c:out value="${receiptList.delivery.route}" /></td>
            <td><c:out value="${receiptList.type}" /></td>
            <td><c:out value="${receiptList.weight}" /></td>
            <td><c:out value="${receiptList.size}" /></td>
            <td><c:out value="${receiptList.price}" /></td>
            <td><c:out value="${receiptList.createTime}" /></td>
            <td><c:out value="${receiptList.updateTime}" /></td>
            <td><c:out value="${receiptList.payStatusName}" /></td>
            <td><c:out value="${receiptList.statusName}" /></td>
            <td>
			<a href='<c:url value="/payOrder?receiptId=${receiptList.id}" />'><fmt:message key="user_page.cart"/></a>
			</td>
            <td>
            <form method="post" action='<c:url value="/cancelOrder?receiptId=${receiptList.id}" />' style="display:inline;">
				<input type="hidden" name="id" value="${receiptList.id}">
				<input type="submit" value="Cancel">
    		</form>
            </td>
            </tr>
        </c:forEach>
       </table>
</h3>
<a href="${pageContext.request.contextPath}/user/user_profile.jsp"><fmt:message key="user_page.return_profile"/></a>
</body>
</html>