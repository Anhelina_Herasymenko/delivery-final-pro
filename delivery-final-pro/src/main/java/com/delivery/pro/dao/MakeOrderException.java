package com.delivery.pro.dao;

public class MakeOrderException extends Exception {
	private static final long serialVersionUID = 1L;

	public MakeOrderException(String message) {
		super(message);
	}
}