package com.delivery.pro.filters;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;

@WebFilter(urlPatterns = {"/*"})
public class LocaleFilter implements Filter {
	FilterConfig filterConfig;

	public void init(FilterConfig fConfig) throws ServletException {
		filterConfig = fConfig;
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		if(httpServletRequest.getParameter("sessionLocale") != null) {
			httpServletRequest.getSession().setAttribute("lang", httpServletRequest.getParameter("sessionLocale"));
		}
		chain.doFilter(request, response);
	}
}