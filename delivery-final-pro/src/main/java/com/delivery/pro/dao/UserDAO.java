package com.delivery.pro.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.User;

import at.favre.lib.crypto.bcrypt.BCrypt;

public class UserDAO {
	
	private static final String SELECT_ALL_FROM_USER = "SELECT * FROM user;";
	private static final String SELECT_BY_EMAIL = "SELECT * FROM user WHERE email = ?;";
	private static final String INSERT_USER = "INSERT INTO user (`firstname`, `lastname`, `email`, `password`) VALUES (?, ?, ?, ?);";
	private static final String DELETE_USER = "DELETE FROM user WHERE email=?;";
	private static final String EDIT_USER_INFO = "UPDATE user SET firstname = ?, lastname = ?, email = ? WHERE id = ?;";
	private static final String SELECT_BY_ID = "SELECT * FROM user WHERE id = ?;";
	private static final String GET_USER_COUNT = "SELECT count(user.id) FROM user;";

	private static final Logger log = LogManager.getLogger(UserDAO.class);
	
	
	
	/**
	 * The method is used to get a list of all users
	 * @return
	 */
	public static List<User> getAllUsers() {
		List<User> userList = new ArrayList<>();
		Connection conn = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			conn = DBManager.getInstance().getConnection();
			statement = conn.createStatement();
			resultSet = statement.executeQuery(SELECT_ALL_FROM_USER);
			
			while (resultSet.next()) {
				userList.add(createUserFromDB(resultSet));
			}
		} catch (SQLException se) {
			log.error("Cannot get list of users", se);
		} finally {
			if(resultSet != null) {
				closeResources(resultSet);
			}
			if(statement != null) {
				closeResources(statement);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
		return userList;
	}
	
	/**
	 * The method is used to get a user by email
	 * @param email
	 * @return
	 */
	public static User getUser(String email) {
		User user = User.createUser();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_BY_EMAIL);
			ps.setString(1, email);
			resultSet = ps.executeQuery();
			if(resultSet.next()) {
				user.setId(resultSet.getInt("id"));
				user.setFirstName(resultSet.getString("firstName"));
				user.setLastName(resultSet.getString("lastName"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
				user.setCreateTime(resultSet.getTimestamp("create_time"));
				user.setRole(resultSet.getInt("user_role_id"));
			} else {
				log.info("User does not exist" + email);
			}
		} catch (SQLException se) {
			log.error("Cannot get user by email" + email, se);
		} finally {
			if (resultSet != null) {
				closeResources(resultSet);
			}
			if (ps != null) {
				closeResources(ps);
			}
			if (conn != null) {
				closeResources(conn);
			}
		}
		return user;
	}
	
	/**
	 * The method is used for a registration of a user
	 * @param user
	 */
	public static void insertUser(User user) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(INSERT_USER, Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, user.getFirstName());
			ps.setString(2, user.getLastName());
			ps.setString(3, user.getEmail());
			ps.setString(4, user.getPassword());
			ps.executeUpdate();
		} catch (SQLException se) {
			log.error("Cannot insert user" + user.getEmail(), se);
		} finally {
			if (ps != null) {
				closeResources(ps);
			}
			if (conn != null) {
				closeResources(conn);
			}
		}
	}
	
	/**
	 * The method is used to get an information of a user
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	public static User createUserFromDB(ResultSet resultSet) throws SQLException {
		User user = User.createUser();
		user.setId(resultSet.getInt("id"));
		user.setFirstName(resultSet.getString("firstName"));
		user.setLastName(resultSet.getString("lastName"));
		user.setEmail(resultSet.getString("email"));
		user.setPassword(resultSet.getString("password"));
		user.setCreateTime(resultSet.getTimestamp("create_time"));
		user.setRole(resultSet.getInt("user_role_id"));
		return user;
	}
	
	/**
	 * The method is used to delete a user by email in tests
	 * @param email
	 */
	public static void deleteUserByEmail(String email) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(DELETE_USER);
			ps.setString(3, email);
			ps.executeUpdate();
		} catch (SQLException se) {
			log.error("Cannot delete user with email " + email, se);
		} finally {
			if(ps != null) {
				closeResources(ps);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
	}
	
	/**
	 * The method is used to update user's info
	 * @param user
	 */
	public static void editUserInfo(User user) {  
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(EDIT_USER_INFO);
			ps.setString(1, user.getFirstName());
			ps.setString(2, user.getLastName());
			ps.setString(3, user.getEmail());
			ps.setInt(4, user.getId());
			ps.executeUpdate();
		} catch (SQLException se) {
			log.error("Cannot edit user", se);
		} finally {
			if(ps != null) {
				closeResources(ps);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
	}
	
	/**
	 * The method is used to log in
	 * @param email
	 * @param password
	 * @return
	 * @throws InsertUserException
	 */
	public static User userLogIn(String email, String password) throws InsertUserException {
		if(email != null && password != null) {
			User userCheck = getUser(email);
			
			if(userCheck.getPassword() == null) {
				userCheck.setPassword("1111");
			}
			BCrypt.Result result = BCrypt.verifyer().verify(password.toCharArray(), userCheck.getPassword());
			if(userCheck.getPassword() != null && result.verified) {
				return userCheck;
			} else {
				log.error("Error! User is not in database " + email);
				throw new InsertUserException("Cannot insert user");
			}
		}
		return User.createUser();
	}
	
	/**
	 * The method is used for a validation of a user during a registration
	 * @param user
	 * @return
	 */
	public static boolean validateUser(User user) {
		User userCheck = getUser(user.getEmail());
		if(userCheck.getPassword() == null) {
			userCheck.setPassword("111111");
		}
		BCrypt.Result result = BCrypt.verifyer().verify(user.getPassword().toCharArray(), userCheck.getPassword());
		if(userCheck.getEmail() == null) {
			return false;
		} else if(userCheck.getEmail().equals(user.getEmail()) && result.verified) {
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * The method is used to close resources
	 * @param closeable
	 */
    private static void closeResources(AutoCloseable closeable){
        try {
            closeable.close();
        } catch (Exception e) {
            log.error("Cannot close resource " + closeable.getClass().getName(), e);
        }
    }

    /**
     * The method is used to get a user by id
     * @param id
     * @return
     */
	public static User getUserId(int id) {
			User user = User.createUser();
			Connection conn = null;
			PreparedStatement ps = null;
			ResultSet resultSet = null;
			try {
				conn = DBManager.getInstance().getConnection();
				ps = conn.prepareStatement(SELECT_BY_ID);
				ps.setInt(1, id);
				resultSet = ps.executeQuery();
				if(resultSet.next()) {
					user.setId(resultSet.getInt("id"));
					user.setFirstName(resultSet.getString("firstName"));
					user.setLastName(resultSet.getString("lastName"));
					user.setEmail(resultSet.getString("email"));
					user.setPassword(resultSet.getString("password"));
					user.setCreateTime(resultSet.getTimestamp("create_time"));
					user.setRole(resultSet.getInt("user_role_id"));
				} else {
					log.info("User does not exist" + id);
				}
			} catch (SQLException se) {
				log.error("Cannot get user by id" + id, se);
			} finally {
				if (resultSet != null) {
					closeResources(resultSet);
				}
				if (ps != null) {
					closeResources(ps);
				}
				if (conn != null) {
					closeResources(conn);
				}
			}
			return user;
		}
	
	/**
	 * The method is used to get user's id in ReceiptDAO
	 * @param id
	 * @return
	 */
	public static int getUserIdInt(int id) {
		User user = User.createUser();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_BY_ID);
			ps.setInt(1, id);
			resultSet = ps.executeQuery();
			if(resultSet.next()) {
				user.setId(resultSet.getInt("id"));
				user.setFirstName(resultSet.getString("firstName"));
				user.setLastName(resultSet.getString("lastName"));
				user.setEmail(resultSet.getString("email"));
				user.setPassword(resultSet.getString("password"));
				user.setCreateTime(resultSet.getTimestamp("create_time"));
				user.setRole(resultSet.getInt("user_role_id"));
			} else {
				log.info("User does not exist" + id);
			}
		} catch (SQLException se) {
			log.error("Cannot get user by id" + id, se);
		} finally {
			if (resultSet != null) {
				closeResources(resultSet);
			}
			if (ps != null) {
				closeResources(ps);
			}
			if (conn != null) {
				closeResources(conn);
			}
		}
		return id;
	}

	public static int getUserCount() {
		int count =1;
		Connection conn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
         conn = DBManager.getInstance().getConnection();
         ps = conn.prepareStatement(GET_USER_COUNT);
         resultSet = ps.executeQuery();
            if (resultSet.next()){
                count = resultSet.getInt(1);
            }
        } catch (SQLException se) {
            log.error("Cannot get user count", se);
        } finally {
            if (resultSet != null){
                closeResources(resultSet);
            }
            if (ps != null){
                closeResources(ps);
            }
            if (conn != null){
                closeResources(conn);
            }
        }
        return count;
	}

	/**
	 * The method is used to get list of users with a pagination
	 * @param startLimit
	 * @param endLimit
	 * @return
	 */
	public static List<User> getAllUsersManager(int startLimit, int endLimit) {
		String SELECT_ALL_USERS_BY_ORDER = "SELECT distinct user.id, user.firstname, user.lastname, user.email, user.create_time "
				+ "FROM user "
				+ "ORDER BY user.id LIMIT ?,?;";
		List<User> userList = new ArrayList<>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_ALL_USERS_BY_ORDER);
			ps.setInt(1, startLimit);
			ps.setInt(2, endLimit);
			resultSet = ps.executeQuery();
			while(resultSet.next()) {
				userList.add(createUserFromDB2(resultSet));
			}
		} catch (SQLException se) {
			log.error("Cannot get a user list", se);
		} finally {
			if(resultSet != null) {
				closeResources(resultSet);
			} 
			if(ps != null) {
				closeResources(ps);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
		return userList;
	}

	private static User createUserFromDB2(ResultSet resultSet) throws SQLException {
			User user = User.createUser();
			user.setId(resultSet.getInt("id"));
			user.setFirstName(resultSet.getString("firstName"));
			user.setLastName(resultSet.getString("lastName"));
			user.setEmail(resultSet.getString("email"));
			user.setCreateTime(resultSet.getTimestamp("create_time"));
			return user;
	}
}