<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">
<head>
	<title><fmt:message key="login_page.title"/></title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
	<jsp:include page="/navigation/navigation_guest.jsp"/>
</head>
<body class="text-center">
<main class="form-login">
<form action="${pageContext.request.contextPath}/Login" method="post">
<h2><fmt:message key="login_page.form_title"/></h2><br>
<p><fmt:message key="user_page.order_fields"/></p><br>
    <b><fmt:message key="login_page.insert_email"/></b><br>
    <input pattern="^([\\w-\\.]+){1,64}@([\\w&&[^_]]+){2,255}.[a-z]{2,}$" type="text" name="email" placeholder="example@gmail.com" required/><br>
    <b><fmt:message key="login_page.insert_password"/></b><br>
    <input type="password" name="password" placeholder="<fmt:message key="register_page.password"/>" required/><br>
    <input type="hidden" name="log_in_form">
    <input type="submit" value="<fmt:message key="login_page.submit"/>"/><br>
</form>
</main>
</body>
</html>