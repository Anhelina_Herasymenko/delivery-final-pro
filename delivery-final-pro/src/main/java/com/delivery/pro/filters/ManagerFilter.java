package com.delivery.pro.filters;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.delivery.pro.entity.User;
@WebFilter(urlPatterns = {"/manager/*"},dispatcherTypes = DispatcherType.FORWARD)
public class ManagerFilter implements Filter {
	FilterConfig filterConfig;
	
	public void init(FilterConfig fConfig) throws ServletException {
		filterConfig = fConfig;
	}
	
	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest requestHTTP = (HttpServletRequest) request;
		HttpSession session = requestHTTP.getSession(true);
		if(!session.isNew() && session.getAttribute("user") != null) {
			User user = (User) session.getAttribute("user");
			if(user.getRole() == User.ROLE_MANAGER) {
				chain.doFilter(request, response);
			} else {
				session.invalidate();
				request.getRequestDispatcher("/error_pages/permission_error.jsp").forward(request, response);
			}
		} 
	}
}