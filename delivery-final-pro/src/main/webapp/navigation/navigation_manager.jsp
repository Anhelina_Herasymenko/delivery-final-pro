<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>

<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/navigation_style.css">
</head>
<body>
<div class="navigation">
	<a href="${pageContext.request.contextPath}/manager/manager_profile.jsp"><fmt:message key="manager_page.profile"/></a>
    <a href="${pageContext.request.contextPath}/UserList"><fmt:message key="manager_page.user_list"/></a>
    <a href="${pageContext.request.contextPath}/loadOrdersManager"><fmt:message key="manager_page.receipt_list_by_date"/></a>
    <a href="${pageContext.request.contextPath}/orderListManager"><fmt:message key="manager_page.receipt_list_by_route"/></a>
    <a href="${pageContext.request.contextPath}/LoadDeliveryManager"><fmt:message key="manager_page.delivery_list_by_directions"/></a>
    <a href="${pageContext.request.contextPath}/manager/addnewdelivery.jsp"><fmt:message key="manager_page.add_new_delivery"/></a>
	<a href="${pageContext.request.contextPath}/LogOut"><fmt:message key="user_navigation.log_out"/></a>
	<a href="?sessionLocale=en"><fmt:message key="user_navigation.locale_en"/></a>
	<a href="?sessionLocale=uk"><fmt:message key="user_navigation.locale_uk"/></a>
</div>
</body>
</html>