package com.delivery.pro.servlets;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.delivery.pro.dao.ReceiptDAO;
import com.delivery.pro.entity.Receipt;

@WebServlet("/loadOrdersManager")
public class LoadOrdersManager extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		List<Receipt> receiptArray;
		HttpSession session = request.getSession();
		if(session.getAttribute("user") == null) {
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
        int allReceipts = ReceiptDAO.getOrderCount();
        int receiptPerPage = 5;
		int numOfPages = (int) Math.ceil((double) allReceipts/receiptPerPage);
		request.setAttribute("numOfPages", numOfPages);
		request.setAttribute("receiptPerPage", receiptPerPage);
		
		if(request.getParameter("currentPage") == null) {
			request.setAttribute("currentPage", 1);
			receiptArray = ReceiptDAO.getAllOrdersManager(0, receiptPerPage);
		} else {
			request.setAttribute("currentPage", request.getParameter("currentPage"));
			int currentPage = Integer.parseInt(request.getParameter("currentPage"));
			receiptArray = ReceiptDAO.getAllOrdersManager((currentPage-1)*receiptPerPage, receiptPerPage);
		} 
		request.setAttribute("receiptArray", receiptArray);
		request.getRequestDispatcher("manager/orders.jsp").forward(request, response);
	}

}
