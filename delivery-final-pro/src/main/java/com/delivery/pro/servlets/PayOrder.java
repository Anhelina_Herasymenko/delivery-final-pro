package com.delivery.pro.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.Receipt;

@WebServlet("/payOrder")
public class PayOrder extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(PayOrder.class);
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Receipt receipt = null;
		int receiptId = Integer.parseInt(request.getParameter("receiptId"));
		
		if(session.getAttribute("user") == null) {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		Connection conn = null;
		try {
			conn = DBManager.getInstance().getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT * FROM receipt WHERE id=?;");
			ps.setInt(1, receiptId);
			ResultSet rs = ps.executeQuery();
			
			if(rs.next()) {
				receipt = Receipt.createReceipt();
				receipt.setId(rs.getInt("id"));
				receipt.setPrice(rs.getDouble("price"));
			}
			rs.close();
			ps.close();
			conn.close();
		} catch (SQLException ex) {
			log.error("Cannot get order with id=" + receiptId, ex);
		}
		request.setAttribute("receipt", receipt);
		request.getRequestDispatcher("/user/pay_order.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int receiptId = Integer.parseInt(request.getParameter("receiptId"));
		HttpSession session = request.getSession();
		if(session.getAttribute("user") == null) {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		Connection conn = null;
		try {
			conn = DBManager.getInstance().getConnection();
			PreparedStatement ps = conn.prepareStatement("UPDATE receipt SET pay_status_id=2 WHERE id=?;");
			ps.setInt(1, receiptId);
			ps.executeUpdate();
			ps.close();
			conn.close();
		} catch (SQLException ex) {
			log.error("Cannot pay order with id=" + receiptId, ex);
		}
		response.sendRedirect("user/result_pay.jsp");
	}
}