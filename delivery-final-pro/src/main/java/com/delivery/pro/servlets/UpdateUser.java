package com.delivery.pro.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.dao.UserDAO;
import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.User;

@WebServlet("/updateUser")
public class UpdateUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(UpdateUser.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		if (session.getAttribute("user") == null) {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		User editUser = null;
		int userId = Integer.parseInt(request.getParameter("userId"));
		System.out.println("userId = " + userId);

		Connection conn = null;
		try {
			conn = DBManager.getInstance().getConnection();
			PreparedStatement pstmt = conn.prepareStatement("SELECT * FROM user WHERE id=?;");
			pstmt.setInt(1, userId);
			ResultSet rs = pstmt.executeQuery();

			while (rs.next()) {
				editUser = User.createUser();
				editUser.setId(rs.getInt("id"));
				editUser.setFirstName(rs.getString("firstName"));
				editUser.setLastName(rs.getString("lastName"));
				editUser.setEmail(rs.getString("email"));
			}
			conn.close();
			rs.close();
			pstmt.close();
		} catch (SQLException ex) {
			log.error("Cannot get userId", ex);
		}

		request.setAttribute("editUser", editUser);
		System.out.println("user = " + editUser);
		request.getRequestDispatcher("/manager/update_user_info.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		int userId = Integer.parseInt(request.getParameter("userId"));
		System.out.println("userId = " + userId);
		User editUser = null;
		if (editUser == null) {
			editUser = User.createUser();
			editUser.setFirstName(request.getParameter("firstName"));
			editUser.setLastName(request.getParameter("lastName"));
			editUser.setEmail(request.getParameter("email"));
			editUser.setId(userId);
			session.setAttribute("editUser", editUser);
		}
		UserDAO.editUserInfo(editUser);
		System.out.println("editUser = " + editUser);
		log.info("Edited User " + editUser.getEmail() + " has been updated");
		response.sendRedirect("manager/result_of_updating.jsp");
	}
}
