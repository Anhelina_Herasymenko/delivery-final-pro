package com.delivery.pro.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.dao.DeliveryDAO;
import com.delivery.pro.dao.UserDAO;
import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.Receipt;

@WebServlet("/getUserOrderInfo")
public class UserOrderInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(UserOrderInfo.class);
       
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		if(session.getAttribute("user") == null) {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		int userId = Integer.parseInt(request.getParameter("userId"));
		System.out.println("userId = " + userId);
		
		List<Receipt> receiptList = new ArrayList<>();
		Connection conn = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getInstance().getConnection();
			PreparedStatement ps = conn.prepareStatement("SELECT distinct receipt.id, user.id, user.email, delivery_id, delivery.route, receipt.type, receipt.weight, receipt.size, receipt.price, receipt.create_time, receipt.update_time,  receipt.status_id, receipt.pay_status_id "
					+ "FROM user, receipt, delivery, status, pay_status "
					+ "WHERE user.id = user_id and delivery.id = delivery_id AND user.id=?;");
			ps.setInt(1, userId);
			System.out.println("userIdTRY = " + userId);
			rs = ps.executeQuery();
			while (rs.next()) {
				receiptList.add(createReceiptUser(rs));
			}
			conn.close();
			rs.close();
			ps.close();
		} catch(SQLException ex) {
			log.error("Cannot get list of orders", ex);
		}
		request.setAttribute("receipts", receiptList);
		System.out.println("receipts=" + receiptList);
		request.getRequestDispatcher("/manager/user_order_list.jsp").forward(request, response);
	}

	private Receipt createReceiptUser(ResultSet rs) throws SQLException {
		Receipt receipt = Receipt.createReceipt();
		receipt.setId(rs.getInt("id"));
		receipt.setUser(UserDAO.getUserId(rs.getInt("id")));
		receipt.setUser(UserDAO.getUser(rs.getString("email")));
		receipt.setDelivery(DeliveryDAO.getDeliveryId(rs.getInt("id")));
		receipt.setDelivery(DeliveryDAO.getDeliveryByRoute(rs.getString("route")));
		receipt.setType(rs.getString("type"));
		receipt.setWeight(rs.getDouble("weight"));
		receipt.setSize(rs.getString("size"));
		receipt.setPrice(rs.getDouble("price"));
		receipt.setCreateTime(rs.getTimestamp("create_time"));
		receipt.setUpdateTime(rs.getTimestamp("update_time"));
		receipt.setPayStatus(rs.getInt("pay_status_id"));
		receipt.setStatus(rs.getInt("status_id"));
		return receipt;
	}
}