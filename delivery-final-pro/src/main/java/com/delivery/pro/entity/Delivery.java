package com.delivery.pro.entity;

import java.util.Objects;

public class Delivery {
	private int id;
	private String route;
	private String weightLimit;
	private String size;
	private double tarif;
	private String description;
	private int category;
	private String categoryName;
	
	public Delivery(){}
	
	public static Delivery createDelivery() {
		return new Delivery();
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getRoute() {
		return route;
	}
	public void setRoute(String route) {
		this.route = route;
	}
	public String getSize() {
		return size;
	}
	public void setSize(String size) {
		this.size = size;
	}
	public String getWeightLimit() {
		return weightLimit;
	}
	public void setWeightLimit(String weightLimit) {
		this.weightLimit = weightLimit;
	}
	public double getTarif() {
		return tarif;
	}
	public void setTarif(double tarif) {
		this.tarif = tarif;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	  public int getCategory() {
		  return category; 
		  } 
	  public void setCategory(int category) {
		  this.category = category; 
		  }
	 
	
	@Override
	public String toString() {
		return "Delivery{" 
				+ "id=" + id 
				+ ", route=" + route 
				+ ", weightLimit=" + weightLimit 
				+ ", size=" + size 
				+ ", tarif=" + tarif
				+ ", description=" + description
				+ ", category=" + category
				+ ", categoryName=" + categoryName
				+ "}";
	}
	
    @Override
    public int hashCode() {
        return Objects.hash(route, tarif);
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Delivery delivery = (Delivery) o;
        return tarif == delivery.tarif && route.equals(delivery.route);
    }

    public String getCategoryName() {
    	return categoryName;
    }
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
		
	}
}