package com.delivery.pro.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.database.DBManager;

@WebServlet("/deleteUser")
public class DeleteUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(DeleteUser.class);
	
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		int userId = Integer.parseInt(request.getParameter("userId"));
	if(session.getAttribute("user") == null) {
		request.getRequestDispatcher("index.jsp").forward(request, response);
	}
		Connection conn = null;
		try {
			conn = DBManager.getInstance().getConnection();
			PreparedStatement pstmt = conn.prepareStatement("DELETE FROM user WHERE id=?;");
			pstmt.setInt(1, userId);
			pstmt.executeUpdate();
			conn.close();
			pstmt.close();
		} catch (SQLException ex) {
			log.error("Cannot delete user with id " + userId, ex);
		}
		response.sendRedirect("manager/result_of_deleting.jsp");
	}
}