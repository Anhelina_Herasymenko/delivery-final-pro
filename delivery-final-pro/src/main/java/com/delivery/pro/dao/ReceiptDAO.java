package com.delivery.pro.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.Receipt;

public class ReceiptDAO {
	
	private static final String SELECT_ALL_FROM_ORDERS = "SELECT distinct receipt.id, user.id, user.email, delivery_id, delivery.route, receipt.type, receipt.weight, receipt.size, receipt.price, receipt.create_time, receipt.update_time, receipt.pay_status_id, pay_status.name, receipt.status_id, status.name, receipt.category_id, category.name "
			+ "FROM user, receipt, delivery, status, pay_status, category "
			+ "WHERE user_id = user.id and delivery_id = delivery.id and receipt.status_id = status.id and receipt.pay_status_id = pay_status.id AND receipt.category_id = category.id "
			+ "ORDER BY receipt.create_time DESC;";
	
	private static final String SELECT_ALL_FROM_ORDERS2 = "SELECT distinct receipt.id, user.id, user.email, delivery_id, delivery.route, receipt.type, receipt.weight, receipt.size, receipt.price, receipt.create_time, receipt.update_time, receipt.pay_status_id, pay_status.name, receipt.status_id, status.name, receipt.category_id, category.name "
			+ "FROM user, receipt, delivery, status, pay_status, category "
			+ "WHERE user_id = user.id and delivery_id = delivery.id and receipt.status_id = status.id and receipt.pay_status_id = pay_status.id AND receipt.category_id=category.id "
			+ "ORDER BY delivery.route ASC;";
	
	private static final String ADD_ORDER_TO_DB = "INSERT INTO receipt (`user_id`, `delivery_id`, `type`, `weight`, `size`) VALUES (?, ?, ?, ?, ?);";
	
	private static final String GET_ORDER_COUNT = "SELECT count(receipt.id) FROM receipt;";
	private static final String GET_ALL_CATEGORY = "SELECT * FROM category;";
	private static final String GET_RECEIPT_COUNT = "SELECT count(id) from receipt WHERE category_id = ?;";
	private static final String GET_USER_RECEIPTS_COUNT = "SELECT count(receipt.id) FROM receipt WHERE user_id=?;";
	
	private static final Logger log = LogManager.getLogger(ReceiptDAO.class);
	
	/**
	 * The method is used to get list of all orders from database for a manager
	 * @param 
	 */
	public static List<Receipt> getAllOrders() {
		List<Receipt> receiptList = new ArrayList<>();
		Connection conn = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			conn = DBManager.getInstance().getConnection();
			statement = conn.createStatement();
			resultSet = statement.executeQuery(SELECT_ALL_FROM_ORDERS);
			while (resultSet.next()) {
				receiptList.add(createReceiptFromDB(resultSet));
			}
		} catch (SQLException se) {
			log.error("Cannot get list of orders", se);
		} finally {
			if(resultSet != null) {
				closeResources(resultSet);
			}
			if(statement != null) {
				closeResources(statement);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
		return receiptList;
	}
	
	/**
	 * The method is used to get all order by route from a database for a manager
	 * @return
	 */
	public static List<Receipt> getAllOrdersByRoute() {
		List<Receipt> receiptList = new ArrayList<>();
		Connection conn = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			conn = DBManager.getInstance().getConnection();
			statement = conn.createStatement();
			resultSet = statement.executeQuery(SELECT_ALL_FROM_ORDERS2);
			while (resultSet.next()) {
				receiptList.add(createReceiptFromDB2(resultSet));
			}
		} catch (SQLException se) {
			log.error("Cannot get list of orders", se);
		} finally {
			if(resultSet != null) {
				closeResources(resultSet);
			}
			if(statement != null) {
				closeResources(statement);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
		return receiptList;
	}
	
	/**
	 * The method is used to create an order in the method getAllOrders
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	public static Receipt createReceiptFromDB(ResultSet resultSet) throws SQLException {
		Receipt receipt = Receipt.createReceipt();
		receipt.setId(resultSet.getInt("id"));
		receipt.setUser(UserDAO.getUserId(resultSet.getInt("id")));
		receipt.setUser(UserDAO.getUser(resultSet.getString("email")));
		receipt.setDelivery(DeliveryDAO.getDeliveryId(resultSet.getInt("id")));
		receipt.setDelivery(DeliveryDAO.getDeliveryByRoute(resultSet.getString("route")));
		receipt.setType(resultSet.getString("type"));
		receipt.setWeight(resultSet.getDouble("weight"));
		receipt.setSize(resultSet.getString("size"));
		receipt.setPrice(resultSet.getDouble("price"));
		receipt.setCreateTime(resultSet.getTimestamp("create_time"));
		receipt.setUpdateTime(resultSet.getTimestamp("update_time"));
		receipt.setPayStatus(resultSet.getInt("pay_status_id"));
		receipt.setPayStatusName(resultSet.getString("pay_status.name"));
		receipt.setStatus(resultSet.getInt("status_id"));
		receipt.setStatusName(resultSet.getString("status.name"));
		receipt.setCategory(resultSet.getInt("category_id"));
		receipt.setCategoryName(resultSet.getString("category.name"));
		return receipt;
	}
	
	/**
	 * The method is used to create an order in the method getAllOrdersByRoute
	 * @param resultSet
	 * @return
	 * @throws SQLException
	 */
	public static Receipt createReceiptFromDB2(ResultSet resultSet) throws SQLException {
		Receipt receipt = Receipt.createReceipt();
		receipt.setId(resultSet.getInt("id"));
		receipt.setUser(UserDAO.getUserId(resultSet.getInt("id")));
		receipt.setUser(UserDAO.getUser(resultSet.getString("email")));
		receipt.setDelivery(DeliveryDAO.getDeliveryId(resultSet.getInt("id")));
		receipt.setDelivery(DeliveryDAO.getDeliveryByRoute(resultSet.getString("route")));
		receipt.setType(resultSet.getString("type"));
		receipt.setWeight(resultSet.getDouble("weight"));
		receipt.setSize(resultSet.getString("size"));
		receipt.setPrice(resultSet.getDouble("price"));
		receipt.setCreateTime(resultSet.getTimestamp("create_time"));
		receipt.setUpdateTime(resultSet.getTimestamp("update_time"));
		receipt.setPayStatus(resultSet.getInt("pay_status_id"));
		receipt.setPayStatusName(resultSet.getString("pay_status.name"));
		receipt.setStatus(resultSet.getInt("status_id"));
		receipt.setStatusName(resultSet.getString("status.name"));
		receipt.setCategory(resultSet.getInt("category_id"));
		receipt.setCategoryName(resultSet.getString("category.name"));
		return receipt;
	}
	
	/**
	 * The method is used to make an order
	 * @param receipt
	 * @param userId
	 * @param deliveryId
	 */
	public static void insertReceipt(Receipt receipt, int userId, int deliveryId) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(ADD_ORDER_TO_DB, Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, UserDAO.getUserIdInt(userId));
			ps.setInt(2, DeliveryDAO.getDeliveryIdInt(deliveryId));
			ps.setString(3, receipt.getType());
			ps.setDouble(4, receipt.getWeight());
			ps.setString(5, receipt.getSize());
			ps.executeUpdate();
		} catch (SQLException se) {
			log.error("Cannot insert order ", se);
		} finally {
			if(ps != null) {
				closeResources(ps);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
	}
	
	/**
	 * The method is used to close all resources
	 * @param closeable
	 */
    private static void closeResources(AutoCloseable closeable){
        try {
            closeable.close();
        } catch (Exception e) {
            log.error("Cannot close resource " + closeable.getClass().getName(),e);
        }
    }

    
    /**
     * The method is used to edit a status of an order
     * @param receipt
     */
	public static void editOrderInfo(Receipt receipt) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement("UPDATE receipt SET status_id=? WHERE id=?;");
			ps.setInt(1, receipt.getStatus());
			ps.setInt(2, receipt.getId());
			ps.executeUpdate();
		} catch (SQLException ex) {
			log.error("Cannot edit an order", ex);
		} finally {
			if(ps != null) {
				closeResources(ps);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
	}

	public static int getOrderCount() {  
		int count=1;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
         conn = DBManager.getInstance().getConnection();
         ps = conn.prepareStatement(GET_ORDER_COUNT);
         resultSet = ps.executeQuery();
            if (resultSet.next()){
                count = resultSet.getInt(1);
            }
        } catch (SQLException se) {
            log.error("Cannot get order count", se);
        } finally {
            if (resultSet != null){
                closeResources(resultSet);
            }
            if (ps != null){
                closeResources(ps);
            }
            if (conn != null){
                closeResources(conn);
            }
        }
        return count;
	}

	public static List<Receipt> getAllOrdersManager(int startLimit, int endLimit) {
			String SELECT_ALL_ORDERS_BY_ORDER = "SELECT distinct receipt.id, user.id, user.email, delivery_id, delivery.route, receipt.type, receipt.weight, receipt.size, receipt.price, receipt.create_time, receipt.update_time, receipt.pay_status_id, pay_status.name, receipt.status_id, status.name, receipt.category_id, category.name "
					+ "FROM user, receipt, delivery, status, pay_status, category "
					+ "WHERE user_id = user.id and delivery_id = delivery.id and receipt.status_id = status.id and receipt.pay_status_id = pay_status.id AND receipt.category_id=category.id "
					+ "ORDER BY receipt.id DESC LIMIT ?,?;";
			List<Receipt> receiptList = new ArrayList<>();
			Connection conn = null;
			PreparedStatement ps = null;
			ResultSet resultSet = null;
			try {
				conn = DBManager.getInstance().getConnection();
				ps = conn.prepareStatement(SELECT_ALL_ORDERS_BY_ORDER);
				ps.setInt(1, startLimit);
				ps.setInt(2, endLimit);
				resultSet = ps.executeQuery();
				while(resultSet.next()) {
					receiptList.add(createReceiptFromDB2(resultSet));
				}
			} catch (SQLException se) {
				log.error("Cannot get a receipt list", se);
			} finally {
				if(resultSet != null) {
					closeResources(resultSet);
				} 
				if(ps != null) {
					closeResources(ps);
				}
				if(conn != null) {
					closeResources(conn);
				}
			}
			return receiptList;
	}

	/**
     * The method is used to get all categories of a receipt from the database
     */
	public static Map<Integer, String> getAllCategory() {
		Map<Integer,String> receiptCategory = new HashMap<>();
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;
        try {
        	conn = DBManager.getInstance().getConnection();
			statement = conn.createStatement();
			resultSet = statement.executeQuery(GET_ALL_CATEGORY);
			while (resultSet.next()) {
				receiptCategory.put(resultSet.getInt("id"), resultSet.getString("name"));
			}
        } catch (SQLException ex) {
        	log.error("Cannot get all categories for an order", ex);
        } finally {
			if(resultSet != null) {
				closeResources(resultSet);
			}
			if(statement != null) {
				closeResources(statement);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
		return receiptCategory;
	}

	/**
	 * The method is used to get a count of orders
	 * @param categoryId
	 * @return
	 */
	public static int getReceiptCount(int categoryId) {
		int count=1;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
         conn = DBManager.getInstance().getConnection();
         ps = conn.prepareStatement(GET_RECEIPT_COUNT);
         ps.setInt(1, categoryId);
         resultSet = ps.executeQuery();
            if (resultSet.next()){
                count = resultSet.getInt(1);
            }
        } catch (SQLException se) {
            log.error("Cannot get order count", se);
        } finally {
            if (resultSet != null){
                closeResources(resultSet);
            }
            if (ps != null){
                closeResources(ps);
            }
            if (conn != null){
                closeResources(conn);
            }
        }
        return count;
	}
	
	/**
	 * The method is used to get a list of orders by categories
	 * @param receiptCategoryId
	 * @param startLimit
	 * @param endLimit
	 * @return
	 */
	public static List<Receipt> getAllReceipts(int receiptCategoryId, int startLimit, int endLimit) {
			String SELECT_ALL_ORDERS_BY_ORDER = "SELECT distinct receipt.id, user.id, user.email, delivery_id, delivery.route, receipt.type, receipt.weight, receipt.size, receipt.price, receipt.create_time, receipt.update_time, receipt.pay_status_id, pay_status.name, receipt.status_id, status.name, receipt.category_id, category.name "
					+ "FROM user, receipt, delivery, status, pay_status, category  "
					+ "WHERE user_id = user.id and delivery_id = delivery.id and receipt.status_id = status.id and receipt.pay_status_id = pay_status.id AND receipt.category_id=category.id AND receipt.category_id=? "
					+ "ORDER BY receipt.create_time DESC LIMIT ?,?;";
			List<Receipt> receiptList = new ArrayList<>();
			Connection conn = null;
			PreparedStatement ps = null;
			ResultSet resultSet = null;
			try {
				conn = DBManager.getInstance().getConnection();
				ps = conn.prepareStatement(SELECT_ALL_ORDERS_BY_ORDER);
				ps.setInt(1, receiptCategoryId);
				ps.setInt(2, startLimit);
				ps.setInt(3, endLimit);
				resultSet = ps.executeQuery();
				while(resultSet.next()) {
					receiptList.add(createReceipt(resultSet));
				}
			} catch (SQLException se) {
				log.error("Cannot get a list of orders", se);
			} finally {
				if(resultSet != null) {
					closeResources(resultSet);
				} 
				if(ps != null) {
					closeResources(ps);
				}
				if(conn != null) {
					closeResources(conn);
				}
			}
			return receiptList;
		}

	private static Receipt createReceipt(ResultSet resultSet) throws SQLException {
		Receipt receipt = Receipt.createReceipt();
		receipt.setId(resultSet.getInt("id"));
		receipt.setUser(UserDAO.getUserId(resultSet.getInt("id")));
		receipt.setUser(UserDAO.getUser(resultSet.getString("email")));
		receipt.setDelivery(DeliveryDAO.getDeliveryId(resultSet.getInt("id")));
		receipt.setDelivery(DeliveryDAO.getDeliveryByRoute(resultSet.getString("route")));
		receipt.setType(resultSet.getString("type"));
		receipt.setWeight(resultSet.getDouble("weight"));
		receipt.setSize(resultSet.getString("size"));
		receipt.setPrice(resultSet.getDouble("price"));
		receipt.setCreateTime(resultSet.getTimestamp("create_time"));
		receipt.setUpdateTime(resultSet.getTimestamp("update_time"));
		receipt.setPayStatus(resultSet.getInt("pay_status_id"));
		receipt.setPayStatusName(resultSet.getString("pay_status.name"));
		receipt.setStatus(resultSet.getInt("status_id"));
		receipt.setStatusName(resultSet.getString("status.name"));
		receipt.setCategory(resultSet.getInt("category_id"));
		receipt.setCategoryName(resultSet.getString("category.name"));
		return receipt;
	}

	public static List<Receipt> AllOrdersOfUser(int userId, int startLimit, int endLimit) {
		String ALL_RECEIPTS_OF_USER_PAGINATION = "SELECT distinct receipt.id, user.id, user.email, delivery_id, delivery.route, receipt.type, receipt.weight, receipt.size, receipt.price, receipt.create_time, receipt.update_time,  receipt.status_id, receipt.pay_status_id "
				+ "FROM user, receipt, delivery, status, pay_status "
				+ "WHERE user.id = user_id and delivery.id = delivery_id AND user.id=? ORDER BY create_time DESC LIMIT ?,?;";
		List<Receipt> receiptList = new ArrayList<>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(ALL_RECEIPTS_OF_USER_PAGINATION);
			ps.setInt(1, userId);
			ps.setInt(2, startLimit);
			ps.setInt(3, endLimit);
			rs = ps.executeQuery();
			while (rs.next()) {
				receiptList.add(createReceiptUser(rs));
			}
		} catch (SQLException ex) {
			log.error("Cannot get list of orders", ex);
		} finally {
			if(rs!= null) {
				closeResources(rs);
			}
			if(ps!=null) {
				closeResources(ps);
			}
			if(conn!=null) {
				closeResources(conn);
			}
		}
		return receiptList;
	}
	
	public static List<Receipt> AllOrdersOfUser(int userId, String createTimeFrom, String createTimeTo, int startLimit, int endLimit) {
		String ALL_RECEIPTS_PAGINATION = "SELECT distinct receipt.id, user.id, user.email, delivery_id, delivery.route, receipt.type, receipt.weight, receipt.size, receipt.price, receipt.create_time, receipt.update_time,  receipt.status_id, receipt.pay_status_id "
				+ "FROM user, receipt, delivery, status, pay_status "
				+ "WHERE user.id = user_id and delivery.id = delivery_id AND user.id=? AND receipt.create_time BETWEEN ? AND ? ORDER BY create_time DESC LIMIT ?,?;";
		List<Receipt> receiptList = new ArrayList<>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(ALL_RECEIPTS_PAGINATION);
			ps.setInt(1, userId);
			ps.setString(2, createTimeFrom);
			ps.setString(3, createTimeTo);
			ps.setInt(4, startLimit);
			ps.setInt(5, endLimit);
			rs = ps.executeQuery();
			while (rs.next()) {
				receiptList.add(createReceiptUser(rs));
			}
		} catch (SQLException ex) {
			log.error("Cannot get list of orders", ex);
		} finally {
			if(rs!=null) {
				closeResources(rs);
			} 
			if(ps!=null) {
				closeResources(ps);
			}
			if(conn!=null) {
				closeResources(conn);
			}
		}
		return receiptList;
	}
	
	public static Receipt createReceiptUser(ResultSet rs) throws SQLException {
		Receipt receipt = Receipt.createReceipt();
		receipt.setId(rs.getInt("id"));
		receipt.setUser(UserDAO.getUserId(rs.getInt("id")));
		receipt.setUser(UserDAO.getUser(rs.getString("email")));
		receipt.setDelivery(DeliveryDAO.getDeliveryId(rs.getInt("id")));
		receipt.setDelivery(DeliveryDAO.getDeliveryByRoute(rs.getString("route")));
		receipt.setType(rs.getString("type"));
		receipt.setWeight(rs.getDouble("weight"));
		receipt.setSize(rs.getString("size"));
		receipt.setPrice(rs.getDouble("price"));
		receipt.setCreateTime(rs.getTimestamp("create_time"));
		receipt.setUpdateTime(rs.getTimestamp("update_time"));
		receipt.setPayStatus(rs.getInt("pay_status_id"));
		receipt.setStatus(rs.getInt("status_id"));
		return receipt;
	}

	public static int getReceiptsOfUser() {
		int count = 1;
		Connection conn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
         conn = DBManager.getInstance().getConnection();
         ps = conn.prepareStatement(GET_USER_RECEIPTS_COUNT);
         resultSet = ps.executeQuery();
            if (resultSet.next()){
                count = resultSet.getInt(1);
            }
        } catch (SQLException se) {
            log.error("Cannot get user's receipts count", se);
        } finally {
            if (resultSet != null){
                closeResources(resultSet);
            }
            if (ps != null){
                closeResources(ps);
            }
            if (conn != null){
                closeResources(conn);
            }
        }
        return count;
	}

}