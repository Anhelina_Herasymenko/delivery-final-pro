<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">
<c:set var="title" value="All deliveries" scope="page"/>
<head>
<title><fmt:message key="user_navigation.delivery_list"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_manager.jsp"/>
</head>
<body>
<div class="header" align="center">
<h2>Delivery List</h2>

<form action="${pageContext.request.contextPath}/DeliveryListManager" method="get">
<div class="info">
<h3>
    <table id="information" border="1">
        <tr>
            <th>ID</th>
            <th><fmt:message key="user_page.route"/></th>
            <th><fmt:message key="user_page.weightlimit"/></th>
            <th><fmt:message key="user_page.size"/></th>
            <th><fmt:message key="user_page.tarif"/></th>
            <th><fmt:message key="user_page.description"/></th>
            <th><fmt:message key="user_page.category_id"/></th>
            <th><fmt:message key="user_page.category_name"/></th>
            <th><fmt:message key="manager_page.edit"/></th>
            <th><fmt:message key="manager_page.delete"/></th>
        </tr>
        <c:forEach items="${deliveries}" var="deliveryList" >
            <tr>
            <td><c:out value="${deliveryList.id}" /></td>
            <td><c:out value="${deliveryList.route}" /></td>
            <td><c:out value="${deliveryList.weightLimit}" /></td>
            <td><c:out value="${deliveryList.size}" /></td>
            <td><c:out value="${deliveryList.tarif}" /></td>
            <td><c:out value="${deliveryList.description}" /></td>
            <td><c:out value="${deliveryList.category}" /></td>
            <td><c:out value="${deliveryList.categoryName}" /></td>
            <td><a href='<c:url value="/editDelivery?deliveryId=${deliveryList.id}" />'><fmt:message key="manager_page.edit"/></a></td>
            <td>
            <form method="post" action='<c:url value="/deleteDelivery?deliveryId=${deliveryList.id}" />' style="display:inline;">
				<input type="hidden" name="id" value="${deliveryList.id}">
				<input type="submit" value="Delete">
    		</form>
			</td>
            </tr>
        </c:forEach>
       </table>
</h3>
</div>
</form><br>
<a href="${pageContext.request.contextPath}/manager/manager_profile.jsp"><fmt:message key="manager_page.return_profile"/></a><br>
</div>
</body>
</html>