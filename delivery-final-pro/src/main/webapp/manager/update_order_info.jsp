<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">
<head>
<title><fmt:message key="manager_page.orders_edit"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_manager2.jsp"/>
</head>
<body>
<div class="header" align="center">
<h2><fmt:message key="manager_page.user_info"/></h2>
<div class="info">
<a href="/delivery-final-pro/AllReceiptList"><fmt:message key="manager_page.user_list"/></a><br>
<h3>
<form action="editOrderInfo" method="post">
<table>
		<tr><td><fmt:message key="manager_page.orderid"/></td><th><input name="receiptId" value="${receipt.id}" readonly/><th/></tr>
		<tr><td><fmt:message key="manager_page.statusid"/></td><th><input name="status_id" value="${receipt.status}"/><th/></tr>
		<tr><td><input type="submit" value="Save" /><td/><tr/>
</table>
</form>
</h3>
<table border="1">
<tr>
        <th><fmt:message key="manager_page.status_id"/></th>
        <th><fmt:message key="manager_page.status_name"/></th>
</tr>
<tr>
            <td>1</td>
            <td>NEW</td>
</tr>
<tr>
            <td>2</td>
            <td>IN_PROGRESS</td>
</tr>
<tr>
            <td>3</td>
            <td>DONE</td>
</tr>
<tr>
            <td>4</td>
            <td>CANCELLED</td>
</tr>
</table>
<br>
<table border="1">
<tr>
        <th><fmt:message key="manager_page.pay_status_id"/></th>
        <th><fmt:message key="manager_page.pay_status_name"/></th>
</tr>
<tr>
            <td>1</td>
            <td>UNPAID</td>
</tr>
<tr>
            <td>2</td>
            <td>PAID</td>
</tr>
</table>
<a href="${pageContext.request.contextPath}/manager/manager_profile.jsp"><fmt:message key="manager_page.return_profile"/></a><br>
</div>
</div>
</body>
</html>