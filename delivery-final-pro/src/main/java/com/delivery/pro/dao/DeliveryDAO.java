package com.delivery.pro.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.database.DBManager;
import com.delivery.pro.entity.Delivery;

public class DeliveryDAO {
	private static final String SELECT_ALL_FROM_DELIVERY = "SELECT distinct delivery.id, delivery.route, delivery.weight_limit, delivery.size, delivery.tarif, delivery.description, category_id, category.name "
	+ "FROM delivery, category "
	+ "WHERE category_id = category.id "
	+ "ORDER BY id;";
	private static final String SELECT_BY_ROUTE = "SELECT * FROM delivery WHERE route = ?;";
	private static final String ADD_DELIVERY_TO_DB = "INSERT INTO delivery (`route`, `weight_limit`, `size`, `tarif`, `description`, `category_id`) "
			+ "VALUES (?, ?, ?, ?, ?, ?);";
	private static final String DELETE_DELIVERY = "DELETE FROM delivery WHERE id = ?;";
	private static final String EDIT_DELIVERY = "UPDATE delivery SET route=?, weight_limit=?, size=?, tarif=?, description=?, category_id=? WHERE id=?;";
	private static final String GET_ALL_CATEGORY = "SELECT * FROM category;";
	private static final String GET_DELIVERY_COUNT = "SELECT count(id) from delivery WHERE category_id = ?;"; 
	private static final String SELECT_DELIVERY_BY_ID = "SELECT distinct * FROM delivery WHERE id = ?;";
	
	private static final Logger log = LogManager.getLogger(DeliveryDAO.class);
	
	
	/**
	 * The method is used to get all deliveries for a user with sorting, category and pagination
	 * @param deliveryCategoryId is used for selection by category
	 * @param orderBy is used for order
	 * @param startLimit is used for pagination
	 * @param endLimit is used for pagination
	 * @return
	 */
	public static List<Delivery> getAllDelivery(int deliveryCategoryId, String orderBy, int startLimit, int endLimit) {
		String SELECT_ALL_DELIVERY_BY_ORDER = "SELECT * FROM delivery WHERE category_id = ? ORDER BY " + validateData(orderBy) +  " LIMIT ?,?;";
		List<Delivery> deliveryList = new ArrayList<>();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_ALL_DELIVERY_BY_ORDER);
			ps.setInt(1, deliveryCategoryId);
			ps.setInt(2, startLimit);
			ps.setInt(3, endLimit);
			resultSet = ps.executeQuery();
			while(resultSet.next()) {
				deliveryList.add(createDeliveryFromDB(resultSet));
			}
		} catch (SQLException se) {
			log.error("Cannot get a delivery for a user", se);
		} finally {
			if(resultSet != null) {
				closeResources(resultSet);
			} 
			if(ps != null) {
				closeResources(ps);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
		return deliveryList;
	}
	

	/**
	 * The method is used to get a delivery by route
	 */
	public static Delivery getDeliveryByRoute(String route) {
		Delivery delivery = Delivery.createDelivery();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_BY_ROUTE);
			ps.setString(1, route);
			resultSet = ps.executeQuery();
			if(resultSet.next()) {
				delivery.setId(resultSet.getInt("id"));
				delivery.setRoute(resultSet.getString("route"));
				delivery.setWeightLimit(resultSet.getString("weight_limit"));
				delivery.setSize(resultSet.getString("size"));
				delivery.setTarif(resultSet.getInt("tarif"));
				delivery.setDescription(resultSet.getString("description"));
				delivery.setCategory(resultSet.getInt("category_id"));
			} else {
				log.info("Missing delivery with " + route);
			}
		}catch (SQLException se) {
			log.error("Cannot get delivery by route" + route, se);
		} finally {
			if(resultSet != null) {
				closeResources(resultSet);
			}
			if(ps != null) {
				closeResources(ps);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
		return delivery;
	}
	
	/**
	 * The method is used to add new delivery to the database
	 * @param delivery is used to insert a delivery to the database
	 */
	public static void addDeliveryToDB(Delivery delivery) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(ADD_DELIVERY_TO_DB);
			ps.setString(1, delivery.getRoute());
			ps.setString(2, delivery.getWeightLimit());
			ps.setString(3, delivery.getSize());
			ps.setDouble(4, delivery.getTarif());
			ps.setString(5, delivery.getDescription());
			ps.setInt(6, delivery.getCategory());
			ps.executeUpdate();
		} catch (SQLException se) {
			log.error("Cannot add new delivery to database", se);
		} finally {
			if(ps != null) {
				closeResources(ps);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
	}
	
	/**
	 * The method is used to delete a delivery from the database for tests
	 * @param delivery is used to search a delivery in the database
	 */
	public static void deleteDelivery(Delivery delivery) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(DELETE_DELIVERY);
			ps.setString(1, delivery.getRoute());
			ps.executeUpdate();
		} catch (SQLException se) {
			log.error("Cannot delete a delivery", se);
		} finally {
			if(ps != null) {
				closeResources(ps);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
	}
	
	/**
	 * Edit a delivery in the database
	 * @param delivery is used for targeting a delivery
	 */
	public static void editDelivery(Delivery delivery) { 
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(EDIT_DELIVERY);
			ps.setString(1, delivery.getRoute());
			ps.setString(2, delivery.getWeightLimit());
			ps.setString(3, delivery.getSize());
			ps.setDouble(4, delivery.getTarif());
			ps.setString(5, delivery.getDescription());
			ps.setInt(6, delivery.getCategory());
			ps.setInt(7, delivery.getId());
			ps.executeUpdate();
		} catch (SQLException se) {
			log.error("Cannot edit a delivery", se);
		} finally {
			if(ps != null) {
				closeResources(ps);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
	}
	
	/**
	 * This method is used to check a safety concatenation of a string in a query
	 * @param data is used to check a safety
	 */
	private static String validateData(String data) { 
		List<String> sortingList = new ArrayList<>();
		Collections.addAll(sortingList, "route", "tarif");
		if(sortingList.contains(data)) {
			return data;
		} else {
			return "id";
		}
	}
	
    /**
     * The method is used to get all categories of a delivery from the database
     */
	public static Map<Integer, String> getAllCategory() {
		Map<Integer,String> deliveryCategory = new HashMap<>();
        Connection conn = null;
        Statement statement = null;
        ResultSet resultSet = null;
		try {
			conn = DBManager.getInstance().getConnection();
			statement = conn.createStatement();
			resultSet = statement.executeQuery(GET_ALL_CATEGORY);
			while (resultSet.next()) {
				deliveryCategory.put(resultSet.getInt("id"), resultSet.getString("name"));
			}
		} catch (SQLException se) {
			log.error("Cannot get all categories for a delivery", se);
		} finally {
			if(resultSet != null) {
				closeResources(resultSet);
			}
			if(statement != null) {
				closeResources(statement);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
		return deliveryCategory;
	}
	
	/**
	 * Return the number of deliveries in the database by a category, for a pagination
	 * @param categoryId is used to get a count of deliveries by a category
	 * @return 
	 */
	public static int getDeliveryCount(int categoryId) {
		int count=1;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet resultSet = null;
        try {
         conn = DBManager.getInstance().getConnection();
         ps = conn.prepareStatement(GET_DELIVERY_COUNT);
         ps.setInt(1, categoryId);
         resultSet = ps.executeQuery();
            if (resultSet.next()){
                count = resultSet.getInt(1);
            }
        } catch (SQLException se) {
            log.error("Cannot get delivery count", se);
        } finally {
            if (resultSet != null){
                closeResources(resultSet);
            }
            if (ps != null){
                closeResources(ps);
            }
            if (conn != null){
                closeResources(conn);
            }
        }
        return count;
    }
	
	/**
	 * The method is used to close resources
	 * @param closeable
	 */
    private static void closeResources(AutoCloseable closeable){
        try {
            closeable.close();
        } catch (Exception e) {
            log.error("Cannot close resource", e);
        }
    }
    
    /**
     * The method is used to get a delivery by route
     * @param route
     * @return
     */
	public static Delivery getDeliveryRoute(String route) {
		Delivery delivery = Delivery.createDelivery();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_BY_ROUTE);
			ps.setString(1, route);
			resultSet = ps.executeQuery();
			if(resultSet.next()) {
				delivery.setId(resultSet.getInt("id"));
				delivery.setRoute(resultSet.getString("route"));
				delivery.setWeightLimit(resultSet.getString("weight_limit"));
				delivery.setSize(resultSet.getString("size"));
				delivery.setTarif(resultSet.getDouble("tarif"));
				delivery.setDescription(resultSet.getString("description"));
				delivery.setCategory(resultSet.getInt("category_id"));
			} else {
				log.info("Delivery does not exist " + route);
			}
		} catch (SQLException se) {
			log.error("Cannot get delivery by route " + route, se);
		} finally {
			if (resultSet != null) {
				closeResources(resultSet);
			}
			if (ps != null) {
				closeResources(ps);
			}
			if (conn != null) {
				closeResources(conn);
			}
		}
		return delivery;
	}

	
	/**
	 * The method is used to get all deliveries by id
	 * @param id
	 * @return
	 */
	public static Delivery getDeliveryId(int id) {
		Delivery delivery = Delivery.createDelivery();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_DELIVERY_BY_ID);
			ps.setInt(1, id);
			resultSet = ps.executeQuery();
			if(resultSet.next()) {
				delivery.setId(resultSet.getInt("id"));
				delivery.setRoute(resultSet.getString("route"));
				delivery.setWeightLimit(resultSet.getString("weight_limit"));
				delivery.setSize(resultSet.getString("size"));
				delivery.setTarif(resultSet.getDouble("tarif"));
				delivery.setDescription(resultSet.getString("description"));
				delivery.setCategory(resultSet.getInt("category_id"));
			} else {
				log.info("Delivery does not exist " + id);
			}
		} catch (SQLException se) {
			log.error("Cannot get delivery by id " + id, se);
		} finally {
			if (resultSet != null) {
				closeResources(resultSet);
			}
			if (ps != null) {
				closeResources(ps);
			}
			if (conn != null) {
				closeResources(conn);
			}
		}
		return delivery;
	}
	
	/**
	 * The method is used to get all deliveries by id for ReceiptDAO
	 * @param id
	 * @return
	 */
	public static int getDeliveryIdInt(int id) {
		Delivery delivery = Delivery.createDelivery();
		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet resultSet = null;
		try {
			conn = DBManager.getInstance().getConnection();
			ps = conn.prepareStatement(SELECT_DELIVERY_BY_ID);
			ps.setInt(1, id);
			resultSet = ps.executeQuery();
			if(resultSet.next()) {
				delivery.setId(resultSet.getInt("id"));
				delivery.setRoute(resultSet.getString("route"));
				delivery.setWeightLimit(resultSet.getString("weight_limit"));
				delivery.setSize(resultSet.getString("size"));
				delivery.setTarif(resultSet.getDouble("tarif"));
				delivery.setDescription(resultSet.getString("description"));
				delivery.setCategory(resultSet.getInt("category_id"));
			} else {
				log.info("Delivery does not exist " + id);
			}
		} catch (SQLException se) {
			log.error("Cannot get delivery by id " + id, se);
		} finally {
			if (resultSet != null) {
				closeResources(resultSet);
			}
			if (ps != null) {
				closeResources(ps);
			}
			if (conn != null) {
				closeResources(conn);
			}
		}
		return id;
	}
	
	/**
	 * The method is used to get list of all deliveries from the database
	 * @param 
	 */
	public static List<Delivery> selectAllDelivery() {
		List<Delivery> deliveryList = new ArrayList<>();
		Connection conn = null;
		Statement statement = null;
		ResultSet resultSet = null;
		try {
			conn = DBManager.getInstance().getConnection();
			statement = conn.createStatement();
			resultSet = statement.executeQuery(SELECT_ALL_FROM_DELIVERY);
			while (resultSet.next()) {
				deliveryList.add(createDeliveryToDB(resultSet));
			}
		} catch (SQLException se) {
			log.error("Cannot get list of deliveries", se);
		} finally {
			if(resultSet != null) {
				closeResources(resultSet);
			}
			if(statement != null) {
				closeResources(statement);
			}
			if(conn != null) {
				closeResources(conn);
			}
		}
		return deliveryList;
	}
	
	/**
	 * The method is used to create a delivery
	 * @param resultSet
	 */
	private static Delivery createDeliveryToDB(ResultSet resultSet) throws SQLException {
		Delivery delivery = Delivery.createDelivery();
		delivery.setId(resultSet.getInt("id"));
		delivery.setRoute(resultSet.getString("route"));
		delivery.setWeightLimit(resultSet.getString("weight_limit"));
		delivery.setSize(resultSet.getString("size"));
		delivery.setTarif(resultSet.getInt("tarif"));
		delivery.setDescription(resultSet.getString("description"));
		delivery.setCategory(resultSet.getInt("category_id"));
		delivery.setCategoryName(resultSet.getString("category.name"));
		return delivery;
	}
	
	/**
	 * The method is used to create a delivery
	 * @param resultSet
	 */
	private static Delivery createDeliveryFromDB(ResultSet resultSet) throws SQLException {
		Delivery delivery = Delivery.createDelivery();
		delivery.setId(resultSet.getInt("id"));
		delivery.setRoute(resultSet.getString("route"));
		delivery.setWeightLimit(resultSet.getString("weight_limit"));
		delivery.setSize(resultSet.getString("size"));
		delivery.setTarif(resultSet.getInt("tarif"));
		delivery.setDescription(resultSet.getString("description"));
		delivery.setCategory(resultSet.getInt("category_id"));
		return delivery;
	}

}