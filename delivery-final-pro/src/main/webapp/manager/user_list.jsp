<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">

<head>
<title><fmt:message key="manager_page.user_list"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_manager.jsp"/>
</head>
<body>
<div class="header" align="center">

<h1><fmt:message key="manager_page.user_list"/></h1>

<h2>
<form action="${pageContext.request.contextPath}/UserList" method="get">
        <input type="hidden" value="${requestScope.currentPage}" name="currentPage">
    </form>


    <div class="info">
            <c:forEach var="i" begin="1" end="${requestScope.numOfPages}">
                <a href="${pageContext.request.contextPath}/UserList?currentPage=${i}">${i}</a>
            </c:forEach>
    </div>
</h2>

<div class="info">
<h3>
    <table id="information" border="1">
        <tr>
            <th>ID</th>
            <th><fmt:message key="manager_page.users_firstname"/></th>
            <th><fmt:message key="manager_page.users_lastname"/></th>
            <th><fmt:message key="manager_page.users_email"/></th>
            <th><fmt:message key="manager_page.users_create"/></th>
            <th><fmt:message key="manager_page.users_orders"/></th>
            <th><fmt:message key="manager_page.users_orders"/></th>
            <th><fmt:message key="manager_page.user_info"/></th>
			<th><fmt:message key="manager_page.users_edit"/></th>
			<th><fmt:message key="manager_page.delete_user"/></th>
        </tr>
        <c:forEach items="${requestScope.userArray}" var="userList" >
            <tr>
            <td><c:out value="${userList.id}" /></td>
            <td><c:out value="${userList.firstName}" /></td>
            <td><c:out value="${userList.lastName}" /></td>
            <td><c:out value="${userList.email}" /></td>
            <td><c:out value="${userList.createTime}" /></td>
            <td><a href="getUserOrderInfoByDate?userId=${userList.id}"><fmt:message key="manager_page.orders"/></a></td>
            <td><a href="getUserOrderInfoByDatePagination?userId=${userList.id}"><fmt:message key="manager_page.orders"/></a></td>
            <td><a href="getUserInfo?userId=${userList.id}"><fmt:message key="manager_page.user_info"/></a></td>
            <td><a href="updateUser?userId=${userList.id}"><fmt:message key="manager_page.edit"/></a></td>
			<td>
			<form method="post" action='<c:url value="/deleteUser?userId=${userList.id}" />' style="display:inline;">
				<input type="hidden" name="id" value="${userList.id}">
				<input type="submit" value="Delete">
    		</form>
			</td>
            </tr>
        </c:forEach>
       </table>
</h3>
</div>
<br>
<a href="${pageContext.request.contextPath}/manager/manager_profile.jsp"><fmt:message key="manager_page.return_profile"/></a><br>

</div>
</body>
</html>