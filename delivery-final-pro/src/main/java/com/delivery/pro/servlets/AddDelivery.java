package com.delivery.pro.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.delivery.pro.dao.DeliveryDAO;
import com.delivery.pro.entity.Delivery;

@WebServlet("/AddDelivery")
public class AddDelivery extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(AddDelivery.class);

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Delivery delivery = null;
		if (session.getAttribute("user") == null) {
			request.getRequestDispatcher("/index.jsp").forward(request, response);
		}
		if (delivery == null) {
			delivery = Delivery.createDelivery();
			delivery.setRoute(request.getParameter("route"));
			delivery.setWeightLimit(request.getParameter("weightlimit"));
			delivery.setSize(request.getParameter("size"));
			delivery.setTarif(Double.parseDouble(request.getParameter("tarif")));
			delivery.setDescription(request.getParameter("description"));
			delivery.setCategory(Integer.parseInt(request.getParameter("category_id")));
			session.setAttribute("delivery", delivery);
		}
		DeliveryDAO.addDeliveryToDB(delivery);
		log.info("Route " + delivery.getRoute() + " has been saved");
		System.out.println("Route " + delivery.getRoute() + " has been saved");
		response.sendRedirect("manager/result_of_adding.jsp");
	}

}
