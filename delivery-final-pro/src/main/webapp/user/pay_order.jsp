<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">

<head>
<title><fmt:message key="user_page.pay_order.title"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_user2.jsp"/>
</head>
<body>
<h3><fmt:message key="user_page.pay_order.can_pay"/></h3>
<div class="info">
<h3>
    <table id="information" border="1">
    <tr><td><fmt:message key="user_page.order_number"/></td><th><c:out value="${receipt.id}" /></th></tr>
    <tr><td><fmt:message key="user_page.price"/></td><th><c:out value="${receipt.price}" /></th></tr></table>
    <form method="post" action='<c:url value="/payOrder?receiptId=${receipt.id}" />' style="display:inline;">
				<input type="hidden" name="id" value="${receipt.id}">
				<input type="submit" value="Pay order">
    </form>
    
</h3>
</div>
<a href="${pageContext.request.contextPath}/user/user_profile.jsp"><fmt:message key="user_page.return_profile"/></a>			
</body>
</html>