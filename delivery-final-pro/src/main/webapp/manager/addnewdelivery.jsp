<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>
<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">

<head>
<title><fmt:message key="manager_page.add_new_delivery"/></title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_manager.jsp"/>
</head>
<body class="text-center">
<main class="form-add_delivery">
<form action="${pageContext.request.contextPath}/AddDelivery" method="post">
    <div class="info">
        <h2><fmt:message key="manager_page.insert_info"/></h2>
            <p><fmt:message key="user_page.order_fields"/></p>
    <b><fmt:message key="manager_page.insert_info_route"/></b><br>
    <input pattern="^[а-яА-Яa-zA-ZàáâäãåąčćęèéêëėįìíîїіłńòóôöõøùúûüųūÿýżźñçčšžйєÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎЇІĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹЙЄÑßÇŒÆČŠŽ∂ð0-9() ,.'-]+$" type="text" name="route" placeholder="Kyiv-Odessa (Smallsize 0-1 kg)" required/><br>
    
    <b><fmt:message key="manager_page.insert_info_weight_limit"/></b><br>
        <select name="weightlimit" required>
            <option>0...1</option>
            <option>1...5</option>
            <option>5...10</option>
            <option>10...20</option>
            <option>20...50</option>
            <option>50...100</option>
        </select><br>
	<b><fmt:message key="manager_page.insert_info_size"/></b><br>
    <select name="size" required>
            <option>SMALLSIZE</option>
            <option>MIDSIZE</option>
            <option>LARGESIZE</option>
        </select><br>
    <b><fmt:message key="manager_page.insert_info_tarif"/></b><br>
    <input pattern="(\d*\.?\d+)\s?" type="number" step = any name="tarif" placeholder="30.50" required/><br>
       
        <b><fmt:message key="manager_page.insert_info_description"/></b><br>
    	<input pattern="^[а-яА-Яa-zA-ZàáâäãåąčćęèéêëėįìíîїіłńòóôöõøùúûüųūÿýżźñçčšžйєÀÁÂÄÃÅĄĆČĖĘÈÉÊËÌÍÎЇІĮŁŃÒÓÔÖÕØÙÚÛÜŲŪŸÝŻŹЙЄÑßÇŒÆČŠŽ∂ð0-9() ,.'-]+$" type="text" name="description" placeholder="Description"/><br>
     
     <b><fmt:message key="manager_page.insert_info_categoryid"/></b><br> 
     <input pattern="(\d+)" type="number" name="category_id" placeholder="Category №" required/><br> 
    
    <input type="submit" value="<fmt:message key="manager_page.submit"/>"/><br>
            <input type="hidden" name="add_delivery_form">
    </div>
</form>
<a href="${pageContext.request.contextPath}/manager/manager_profile.jsp"><fmt:message key="manager_page.return_profile"/></a><br>
</main>
</body>
</html>