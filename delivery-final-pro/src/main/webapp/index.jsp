<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <title><fmt:message key="index_page.title"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
    <jsp:include page="/navigation/navigation_guest.jsp"/>
</head>
<body>
<div class="header" align="center">
<h1><fmt:message key="index_page.hello"/><br></h1>
<h3>
        <fmt:message key="index_page.chooseLanguage"/>
        <a href="?sessionLocale=en"><fmt:message key="user_navigation.locale_en"/></a>
		<a href="?sessionLocale=uk"><fmt:message key="user_navigation.locale_uk"/></a>
</h3>
<a href="${pageContext.request.contextPath}/"><img alt="logo" src="img/logo.jpg"></a><br>
<h3>
<fmt:message key="index_page.delivery_list_info"/> <a href="${pageContext.request.contextPath}/loadDeliveryList"><fmt:message key="guest_page.delivery"/></a><br>
<fmt:message key="index_page.can_login"/> <a href="login.jsp"><fmt:message key="index_page.login"/></a><br>
<fmt:message key="index_page.or"/><br>
<fmt:message key="index_page.can_register"/> <a href="register.jsp"><fmt:message key="index_page.register"/></a>
</h3>
</div>
</body>
</html>