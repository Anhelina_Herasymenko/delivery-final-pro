<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">
<head>
<title><fmt:message key="error_page.500_title"/></title>
</head>
<body>
<fmt:message key="error_page.500_title"/><br>
<fmt:message key="error_page.500_a"/><br>
<fmt:message key="error_page.500_b"/><br>
<a href="index.jsp"><fmt:message key="error_page.500_c"/></a>
</body>
</html>