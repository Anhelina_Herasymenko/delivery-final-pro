<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.language}">
<head>
    <title><fmt:message key="user_page.cart"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
    <jsp:include page="/navigation/navigation_user.jsp"/>
</head>
<body>
<h3><fmt:message key="user_page.user_cart.success"/><br>
<a href="${pageContext.request.contextPath}/MyOrderInfo?userId=${user.id}"><fmt:message key="user_navigation.order_info"/></a>
</h3>
<a href="${pageContext.request.contextPath}/user/user_profile.jsp"><fmt:message key="user_page.return_profile"/></a>
</body>
</html>