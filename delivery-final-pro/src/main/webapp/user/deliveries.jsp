<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false" %>

<fmt:setLocale value="${sessionScope.lang}"/>
<fmt:setBundle basename="language"/>
<html lang="${param.lang}">
<head>
    <title><fmt:message key="user_navigation.delivery_list"/></title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/styles/style.css">
<jsp:include page="/navigation/navigation_user.jsp"/>
</head>
<body>
<h2><fmt:message key="user_navigation.delivery_list"/></h2>
    <h3><div class="navigation">
        <c:forEach items="${requestScope.deliveryCategory}" var="category">
            <a href="${pageContext.request.contextPath}/LoadDeliveryUser?deliveryCategoryId=${category.key}&deliverySort=${requestScope.deliverySort}&currentPage=1">${category.value}</a>
        </c:forEach>
    </div>
        <form action="${pageContext.request.contextPath}/LoadDeliveryUser" method="get">
        <input type="hidden" value="sort_select">
        <input type="hidden" value="${requestScope.deliveryCategoryId}" name="deliveryCategoryId">
        <input type="hidden" value="${requestScope.currentPage}" name="currentPage">
    </form>
</h3>    
    
<h2>
    <div class="info">
            <c:forEach var="i" begin="1" end="${requestScope.numOfPages}">
                <a href="${pageContext.request.contextPath}/LoadDeliveryUser?currentPage=${i}&deliveryCategoryId=${requestScope.deliveryCategoryId}&deliverySort=${requestScope.deliverySort}">${i}</a>
            </c:forEach>
    </div>
</h2>

 <div class="container">
    <table id="information" border="1">
    	<tr>
            <th>ID</th>
            <th><fmt:message key="guest_page.route"/></th>
            <th><fmt:message key="guest_page.weightlimit"/></th>
            <th><fmt:message key="guest_page.size"/></th>
            <th><fmt:message key="guest_page.tarif"/></th>
            <th><fmt:message key="guest_page.description"/></th>
            <th><fmt:message key="user_page.select"/></th>
        </tr>
        <c:forEach var="deliveries" items="${requestScope.deliveryArray}">
                <tr>
                <td><c:out value="${deliveries.id}" /></td>
                <td><c:out value="${deliveries.route}" /></td>
                <td><c:out value="${deliveries.weightLimit}" /></td>
                <td><c:out value="${deliveries.size}" /></td>
                <td><c:out value="${deliveries.tarif}" /></td>
                <td><c:out value="${deliveries.description}" /></td>
                <td><a href="makeOrder?deliveryId=${deliveries.id}"><fmt:message key="user_page.select"/></a></td>
                </tr>
        </c:forEach>
        </table>
 </div>
 <a href="${pageContext.request.contextPath}/user/user_profile.jsp"><fmt:message key="user_page.return_profile"/></a>
</body>
</html>