package com.delivery.pro.dao;

public class InsertUserException extends Exception {
	private static final long serialVersionUID = 1L;

	public InsertUserException(String message){
        super(message);
    }
}