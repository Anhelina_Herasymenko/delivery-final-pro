package com.delivery.pro.entity;

import java.sql.Timestamp;
import java.util.Objects;

public class User {
	public static final int ROLE_MANAGER = 1;
    public static final int ROLE_USER = 2;
    public static final int ROLE_GUEST = 3;
    
	private int id;
	private String firstName;
	private String lastName;
    private String email;
    private String password;
    private int role = ROLE_USER;
    private Timestamp createTime;
    
    public User(){}
    
    public static User createUser(){
        return new User();
    }
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getRole() {
		return role;
	}
	public void setRole(int role) {
		this.role = role;
	}
	public Timestamp getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Timestamp createTime) {
		this.createTime = createTime;
	}
	
	@Override
    public String toString() {
        return "User{" +
                "id=" + id
                + ", firstName=" + firstName
                + ", lastName=" + lastName 
                + ", email=" + email
                + ", password=" + password
                + ", role=" + role
                + ", createTime=" + createTime
                + "}";
    }
	
    @Override
    public boolean equals(Object o) {

        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return email.equals(user.email) && password.equals(user.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(email, password);
    }
}