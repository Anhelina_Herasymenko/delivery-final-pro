package com.delivery.pro.database;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DBManager {
	private static DBManager instance;
	private boolean isTest;
	private static final Logger log = LogManager.getLogger(DBManager.class);
	private DBManager() {}
	private DBManager(boolean isTest) {
		this.isTest = isTest;
	}
	
	public static DBManager getInstance() {
		if(instance == null) {
			instance = new DBManager();
		}
		return instance;
	}
	
	public static DBManager getInstance(boolean isTest) {
		if(instance == null) {
			instance = new DBManager(isTest);
		}
		return instance;
	}
	
	public Connection getConnection() {
		Context context;
		Connection connection = null;
		if(isTest) {
			return getConnectionForTests();
		}
		try {
			context = new InitialContext();
			DataSource ds = (DataSource)context.lookup("java:comp/env/jdbc/UsersDB");
			connection = ds.getConnection();
			log.info("Get connection by pool");
		} catch (NamingException | SQLException e) {
			log.error("Error with connection pool", e);
		}
		return connection;
	}
	
	private Connection getConnectionForTests() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
		} catch (InstantiationException | InvocationTargetException | NoSuchMethodException | IllegalAccessException | ClassNotFoundException e) {
			log.error("Error with connection for test", e);
		}
		try {
			connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3307/delivery?serverTimezone=Europe/Kiev","root","root");
		} catch (SQLException throwables) {
			log.error("Error with getting connection for tests", throwables);
		}
		return connection;
	}
}